<!DOCTYPE html>
<html>
	<head>
		<title>Layercake</title>
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<link rel="shortcut icon" href="images/favicon.ico">
		<style>
			body {
			font-family: Helvetica, Arial, Sans-Serif;
			}
		
			.switch {
			  position: relative;
			  display: inline-block;
			  width: 42px;
			  height: 25px;
			}

			.switch input {display:none;}

			.slider {
			  position: absolute;
			  cursor: pointer;
			  top: 0;
			  left: 0;
			  right: 0;
			  bottom: 0;
			  background-color: #ccc;
			  -webkit-transition: .4s;
			  transition: .4s;
			}

			.slider:before {
			  position: absolute;
			  content: "";
			  height: 21px;
			  width: 21px;
			  left: 1px;
			  bottom: 2px;
			  background-color: white;
			  -webkit-transition: .4s;
			  transition: .4s;
			}

			input:checked + .slider {
			  background-color: #2196F3;
			}

			input:focus + .slider {
			  box-shadow: 0 0 1px #2196F3;
			}

			input:checked + .slider:before {
			  -webkit-transform: translateX(18px);
			  -ms-transform: translateX(18px);
			  transform: translateX(18px);
			}

			/* Rounded sliders */
			.slider.round {
			  border-radius: 24px;
			}

			.slider.round:before {
			  border-radius: 50%;
			}
		
			/* Always set the map height explicitly to define the size of the div
			* element that contains the map. */
			
			#logo {
			margin: auto;
			vertical-align: middle;
			height: 2em;
			}
			.helper {
			display: inline-block;
			vertical-align: middle;
			height: 100%
			}
			#map {
			height: 100%;
			z-index: 1;
			}
			/* Optional: Makes the sample page fill the window. */
			html, body {
			height: 100%;
			margin: 0;
			padding: 0;
			}
			#navigation {
			height: 4.3em;
			text-align: center;
			white-space: nowrap;
			background-color: #f7f7f7;
			}
			#layersmenu {
			display: none;
			height: 40%;
			width: 100%;
			bottom: 2.5em;
			text-align: center;
			white-space: nowrap;
			position: absolute;
			z-index: 200;

			}
			#layersmenutitlebar {
			height: 2.5em;
			width: 100%;
			background-color: #f7f7f7;
			text-align: right;
			}
			#layermenutitlebarbox {
			width: 3em;
			height: 3em;
			display: inline-block;
			margin: 0;
			background-color: white;
			}
			#layermenutitlebartext {
			width: calc(100% - 6em);
			height: 3em;
			display: inline-block;
			margin: 0;
			background-color: white;
			}
			#closebutton {
			margin-left: auto;
			margin-right: auto;
			position: relative;
			top: 50%;
			transform: translateY(-50%);
			}
			#layersmenudiv {
			height: 100%;
			background-color: white;
			overflow-x: hidden;
			}
			#layersmenutable {
			width:100%;
			table-layout:fixed;
			border-collapse: collapse;
			}
			#layersmenulayertitle {
			text-align:left;
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
			}
			.button {
			margin: auto;
			cursor: pointer;
			vertical-align: middle;
			height: 100%;
			}
			.closebutton {
			margin: 10px;
			cursor: pointer;
			vertical-align: middle;
			font-size: 150%;
			}
		</style>
		<link rel="manifest" href="/manifest.json">
	</head>
	<body>
	<script>
<?php	
	$layers = [		"Eventbrite_SF",
					"Ticketmaster_SF",
					"Meetup_SF",
					"Ford_GoBike",
					"Scoot_Scooters_SF",
					"In_N_Out_Burger_SF",
					"McDonalds_SF",
					"Anthony_Bourdain_The_Layover_S1E5", 
					"Anthony_Bourdain_Parts_Unknown_S6E4",
					"Anthony_Bourdain_No_Reservations_S5E15",
					"Diners_Drive_ins_and_Dives_SF",
					"Man_vs_Food_SF",
					"24_Hour_Fitness_SF",
					"Film_Locations",
					"BringFido_SF"
					];
	$layersJS = json_encode($layers);
	echo "var layers = ". $layersJS . ";\n";
	
	if(isset($_COOKIE['returnVisit'])) //Check if first visit
	{
		$enabledLayers = [];
		foreach ($layers as $layer) {
			if(isset($_COOKIE[$layer]))	{ //Check if cookie is set
				$enabledLayers[] = $layer; // Add to enabledLayers array
			}
		}
		
		$enabledLayersJS = json_encode($enabledLayers);
		echo "var enabledLayers = ". $enabledLayersJS . ";\n";
	}
	else // First visit
	{
		setcookie('returnVisit', '1', 2147483645, '/');
		setcookie('Eventbrite_SF', '1', 2147483645, '/');
		setcookie('Ticketmaster_SF', '1', 2147483645, '/');
		setcookie('Meetup_SF', '1', 2147483645, '/');
		echo "var enabledLayers = [\"Eventbrite_SF\", \"Ticketmaster_SF\", \"Meetup_SF\"];\n";
	}
	
?>
	</script>
		<div id="layersmenu">
			<div id="layersmenutitlebar"><span class="closebutton" onclick="showDiv('layersmenu')">✖</span>
				<!-- <div id="layermenutitlebarbox"></div><div id="layermenutitlebartext"></div><div id="layermenutitlebarbox"><div id="closebutton"></div></div> -->
			</div>
			<div id="layersmenudiv">
				<table id="layersmenutable">
				
<?php					
	$layerIndex = 0;
	$layerBgColor = "";
	$layerCheckedDefault = "";
	foreach ($layers as $layer) {
		if ($layerIndex % 2){ $layerBgColor = "#f5f5f5"; } else { $layerBgColor = "#ffffff"; } // Set alternating background
		
		$layerCheckedDefault = "";
		if(isset($_COOKIE[$layer]) || (!isset($_COOKIE['returnVisit']) && ($layer == "Eventbrite_SF" || $layer == "Ticketmaster_SF" || $layer == "Meetup_SF"))) // I hate this logic but it works for now
		{ $layerCheckedDefault = " checked"; }
		echo "<tr style='background: ".$layerBgColor.";'>
				<td width='40'><img src='./layers/".$layer.".png' height='32' width='32'></td>
				<td id='layersmenulayertitle'>".str_replace( '_', ' ', $layer)."</td>
				<td width='50'><label class='switch'><input id='".$layer."' type='checkbox' onclick=\"(this.checked)?initMap.loadMapLayer('".$layer."'):initMap.removeMapLayer('".$layer."');\"".$layerCheckedDefault."><span class='slider round'></span></label></td></tr>";
		$layerIndex++;
	}
?>
				</table>
			</div>
		</div>
		<div id="map"></div>
		<script>
			function setCookie(name,value) {
				document.cookie = name + "=" + value + "; expires=Tue, 19 Jan 2038 03:14:07 UTC; path=/";
			}
			
			function deleteCookie(name) {
				document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:01 UTC;";
			}
			
			function getCookie(name) {
				var nameEQ = name + "=";
				var ca = document.cookie.split(';');
				for(var i=0;i < ca.length;i++) {
					var c = ca[i];
					while (c.charAt(0)==' ') c = c.substring(1,c.length);
					if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
				}
				return null;
			}

			var openmenu = 0;
			
			// Navigation: 
			function showDiv(divid) {
				if (openmenu == 0)
				{
					document.getElementById(divid).style.display = "block";
					openmenu = 1;
					console.log(openmenu);
				}
				else
				{
					document.getElementById('layersmenu').style.display = "none";
					openmenu = 0;
					console.log(openmenu);
				}
			}
			
			var mapStyle = [
			  {
				"featureType": "administrative",
				"stylers": [{ "visibility": "off" }]
			  },
			  {
				"featureType": "landscape",
				"stylers": [{ "saturation": -50 }]
			  },
			  {
				"featureType": "poi",
				"stylers": [{"visibility": "off"}]
			  },
			  {
				"featureType": "poi.attraction",
				"stylers": [{"visibility": "off"}]
			  },
			  {
				"featureType": "poi.park",
				"stylers": [{"visibility": "simplified"}]
			  },
			  {
				"featureType": "transit",
				"stylers": [{"visibility": "on"}]
			  }
			]
			
			function LayerMenuButtonControl(controlDiv, map) {
				// Set CSS for the control border.
				var controlUI = document.createElement('div');
				controlUI.style.backgroundColor = '#fff';
				controlUI.style.border = '2px solid #fff';
				controlUI.style.borderRadius = '1px';
				controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
				controlUI.style.cursor = 'pointer';
				controlUI.style.marginBottom = '22px';
				controlUI.style.textAlign = 'center';
				controlUI.title = 'Click to open Layer menu';
				controlDiv.appendChild(controlUI);

				// Set CSS for the control interior.
				var controlText = document.createElement('div');
				controlText.style.color = 'rgb(25,25,25)';
				controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
				controlText.style.fontSize = '16px';
				controlText.style.lineHeight = '38px';
				controlText.style.paddingLeft = '5px';
				controlText.style.paddingRight = '5px';
				controlText.innerHTML = 'Subscribed Layers';
				controlUI.appendChild(controlText);

				// Setup the click event listeners: simply set the map to Chicago.
				controlUI.addEventListener('click', function() {
					showDiv('layersmenu');
				});
			}
			
			var map, infoWindow, layerGroups;
			
			function initMap() {
				map = new google.maps.Map(document.getElementById('map'), {
					center: {lat: 37.782000, lng: -122.419416},
					zoom: 15,
					fullscreenControl: false,
					mapTypeControlOptions: { mapTypeIds: [] }, //Remove "Satellite" option
					streetViewControl: false,
					gestureHandling: 'greedy',
					draggable: true,
					styles: mapStyle
				});
				
				var maxWindowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
				maxWindowWidth = maxWindowWidth - 75;
				infoWindow = new google.maps.InfoWindow({
					maxWidth: maxWindowWidth
				});;
				
				var layerMenuButton = document.createElement('div');
				var centerControl = new LayerMenuButtonControl(layerMenuButton, map);
				layerMenuButton.index = 1;
				map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(layerMenuButton);
				
				layerGroups = new google.maps.MVCObject();
				
				function downloadUrl(url, callback) {
					var request = window.ActiveXObject ?
					new ActiveXObject('Microsoft.XMLHTTP') :
					new XMLHttpRequest;
					
					request.onreadystatechange = function() {
						if (request.readyState == 4) {
							callback(request, request.status);
						}
					};
					
					request.open('GET', url, true);
					request.send(null);
				}
				
				//Put all layers on map
				for (var i = 0, len = enabledLayers.length; i < len; i++) {
					loadMapLayer(enabledLayers[i]);
				}
				
				function loadMapLayer(layerName) {
					downloadUrl('./layers/'+layerName+'.xml?dt='+Math.floor(Date.now()/100000), function(data) {
						var xml = data.responseXML;
						var markers = xml.documentElement.getElementsByTagName('marker');

						addMarkersToMap(markers);
					});
					layerGroups.set(layerName, map);
					setCookie(layerName,"1");
				}
				initMap.loadMapLayer = loadMapLayer; // required to call function outside of initMap
				
				function removeMapLayer(layerName)
				{
					layerGroups.set(layerName, null);
					deleteCookie(layerName);
				}
				initMap.removeMapLayer = removeMapLayer; // required to call function outside of initMap

				//Add LayerCake logo to top
				function layercakeLogoControl(controlDiv) {
					controlDiv.style.padding = '12px';
					var logo = document.createElement('IMG');
					logo.src = 'logo.png';
					logo.style.height = '4.5em';
					controlDiv.appendChild(logo);
				}
				var logoControlDiv = document.createElement('DIV');
				var logoControl = new layercakeLogoControl(logoControlDiv);
				logoControlDiv.index = 0; // used for ordering
				map.controls[google.maps.ControlPosition.TOP_CENTER].push(logoControlDiv);
								
				function addMarkersToMap(markers) { 
					Array.prototype.forEach.call(markers, function(markerElem) { //Second anonymous function
						//Set InfoWindow variables
						
						var name = markerElem.getAttribute('name');
						var address = markerElem.getAttribute('address');
						var type = markerElem.getAttribute('type');
						var point = new google.maps.LatLng(parseFloat(markerElem.getAttribute('lat')), parseFloat(markerElem.getAttribute('lng')));
						var info = markerElem.getAttribute('info');
						var source = markerElem.getAttribute('source');
						var infowincontent = document.createElement('div');
						
						//Add Icon to InfoWindow
						var layerIMG = document.createElement('img');
						layerIMG.src = './layers/'+type+'.png';
						layerIMG.style.float = 'left';
						layerIMG.style.marginRight = '10px';
						layerIMG.style.marginBottom = '5px';
						layerIMG.height = 32;
						layerIMG.width = 32;
						layerIMG.alt = type;
						infowincontent.appendChild(layerIMG);
						
						//Add title to InfoWindow
						var strong = document.createElement('strong');
						strong.textContent = name
						infowincontent.appendChild(strong);
						infowincontent.appendChild(document.createElement('br'));
						
						//Add address to InfoWindow
						var addresstext = document.createElement('text');
						addresstext.textContent = address
						addresstext.style.fontStyle = 'italic';
						addresstext.style.fontSize = 'smaller';
						infowincontent.appendChild(addresstext);
						infowincontent.appendChild(document.createElement('br'));
						
						//Add info to InfoWindow			
						if (info) { // Skip if info is blank or null
							var infotext = document.createElement('div');
							infotext.innerHTML = "<img src='info.png' height='16' width='16' alt='More info'>  " + info;
							infowincontent.appendChild(document.createElement('br'));
							infowincontent.appendChild(infotext);
						}
						
						//Add Source link to InfoWindow
						infowincontent.appendChild(document.createElement('br'));
						infowincontent.appendChild(document.createElement('br'));
						var gmapsIMG = document.createElement('img');
						var spacePipeText = document.createElement('text');
						var spaceText = document.createElement('text');
						spaceText.textContent = ' ';
						var spaceText2 = document.createElement('text');
						spaceText2.textContent = ' ';
						var sourceIMG = document.createElement('img');
						var sourcelink = document.createElement('a');
						var gMapsLink = document.createElement('a');
						
						if (source) { // Skip if source is blank or null
							sourcelink.setAttribute('href', source);
							sourcelink.setAttribute('target', '_blank'); 
							sourceIMG.src = 'source.png';
							sourceIMG.height = 16;
							sourceIMG.width = 16;
							sourcelink.innerText = 'Visit Source';
							spacePipeText.textContent = '  |  ';
							
							infowincontent.appendChild(sourceIMG);
							infowincontent.appendChild(spaceText);
							infowincontent.appendChild(sourcelink);
							infowincontent.appendChild(spacePipeText);
						}
						gMapsLink.setAttribute('href','https://www.google.com/maps/place/'+encodeURIComponent(address));
						gMapsLink.setAttribute('target', '_blank'); 
						gMapsLink.innerText = 'View on Google Maps';
						gmapsIMG.src = 'gmaps.png';
						gmapsIMG.height = 16;
						gmapsIMG.width = 16;
						infowincontent.appendChild(gmapsIMG);
						infowincontent.appendChild(spaceText2);
						infowincontent.appendChild(gMapsLink);

						var layerIcon = new google.maps.MarkerImage('./layers/'+type+'.png', null, null, null, new google.maps.Size(28,28));
						
						//Create marker
						var marker = new google.maps.Marker({
							map: map,
							position: point,
							title: type,
							icon: layerIcon
						});
						
						//Pop up InfoWindow when marker clicked
						marker.addListener('click', function() { //Third anonymous function
							infoWindow.setContent(infowincontent);
							infoWindow.open(map, marker);
						});
						
						marker.bindTo('map', layerGroups, type); //type = map layer name
						//console.log('layerGroups ' + Object.keys(layerGroups));
					});
				}
				
				// Add "Your Location" button
				function addYourLocationButton(map, marker) {
					var controlDiv = document.createElement('div');
					
					var firstChild = document.createElement('button');
					firstChild.style.backgroundColor = '#fff';
					firstChild.style.border = 'none';
					firstChild.style.outline = 'none';
					firstChild.style.width = '28px';
					firstChild.style.height = '28px';
					firstChild.style.borderRadius = '2px';
					firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
					firstChild.style.cursor = 'pointer';
					firstChild.style.marginRight = '10px';
					firstChild.style.padding = '0px';
					firstChild.title = 'Your Location';
					controlDiv.appendChild(firstChild);
					
					var secondChild = document.createElement('div');
					secondChild.style.margin = '5px';
					secondChild.style.width = '18px';
					secondChild.style.height = '18px';
					secondChild.style.backgroundImage = 'url(https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-1x.png)';
					secondChild.style.backgroundSize = '180px 18px';
					secondChild.style.backgroundPosition = '0px 0px';
					secondChild.style.backgroundRepeat = 'no-repeat';
					secondChild.id = 'you_location_img';
					firstChild.appendChild(secondChild);
					
					google.maps.event.addListener(map, 'dragend', function() {
						document.getElementById('you_location_img').style.backgroundPosition = '0px 0px';
					});

					firstChild.addEventListener('click', function() {
						var imgX = '0';
						var animationInterval = setInterval(function(){
							if(imgX == '-18') imgX = '0';
							else imgX = '-18';
							document.getElementById('you_location_img').style.backgroundPosition = imgX + 'px 0px';
						}, 500);
						if(navigator.geolocation) {
							navigator.geolocation.getCurrentPosition(function(position) {
								var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
								marker.setPosition(latlng);
								map.setCenter(latlng);
								clearInterval(animationInterval);
								document.getElementById('you_location_img').style.backgroundPosition = '-144px 0px';
							});
						}
						else{
							clearInterval(animationInterval);
							document.getElementById('you_location_img').style.backgroundPosition = '0px 0px';
						}
					});
					
					controlDiv.index = 1;
					map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlDiv);
				}

				// Try HTML5 geolocation.
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function(position) {
						var pos = {
							lat: position.coords.latitude,
							lng: position.coords.longitude
						};
						
						//infoWindow.setPosition(pos);
						//infoWindow.setContent('Location found.');
						//infoWindow.open(map);
						
						//map.setCenter(pos); Re-enable this when HTTPS
						}, function() {
						//handleLocationError(true, infoWindow, map.getCenter());
					});
					} else {
					// Browser doesn't support Geolocation
					//handleLocationError(false, infoWindow, map.getCenter());
				}
				var myMarker = new google.maps.Marker({
					position: {lat: 37.782000, lng: -122.419416}, //position: pos,
					map: map,
					icon: 'bluedot.png'
				});
				addYourLocationButton(map, myMarker);
			}
			
			function handleLocationError(browserHasGeolocation, infoWindow, pos) {
				infoWindow.setPosition(pos);
				infoWindow.setContent(browserHasGeolocation ?
				'Error: The Geolocation service failed.' :
				'Error: Your browser doesn\'t support geolocation.');
				infoWindow.open(map);
			}
		</script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDD4RNMqrsAL9d6M1YgNx0xwFNNiggBhIk&callback=initMap">
		</script>
		<script>
			

		</script>
	</body>
</html>										