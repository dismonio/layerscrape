# -*- coding: utf-8 -*-
"""
Created on Sat Feb 03 18:24:55 2018

@author: Samuel Steele
@email: sam@sam-steele.com
"""

import json #For json parsing
import xml.etree.ElementTree as ET #for XML manipulation
import xml.dom.minidom
from urllib import urlopen #For pulling in URLs from the web
import os #For seeing if files exist
import shutil #For copying files
import requests #For Google Geocode
import time #For waiting between API Calls
from unidecode import unidecode #To clean up strings
import datetime as dt #For converting datetime into something useful (Eventbrite start/end)
from datetime import datetime


def main_execute():
    #Google API keys, these need to be updated when pushed to production
    googleGeocodeKey = 'AIzaSyB8qtqib7zL9Xu4eKAClcXAeWNTTxpREn8'
    googlePlacesKey = 'AIzaSyB_RV9jTPOHd7Xx-KBta-oT0pSrKeNYGJ0'
    
    dir_path = os.path.dirname(os.path.realpath(__file__))
    print "The working directory is: " + dir_path
    
    #Do you want to pull live JSON data?
    importFeed = 1
    
    ############### MANUAL ENTRY REQUIRED HERE##############
    urlBase = 'https://app.ticketmaster.com/discovery/v2/events.json?apikey=Kx3XOaqJUrU80JMG15G7TGIWq7x6iWRn&geoPoint=9q8yy&radius=50'
    markerType = "Ticketmaster_SF"
    xmlFilePrefix = "Ticketmaster_SF"
    xmlFileSuffix = ".xml"
    xmlFilename = xmlFilePrefix + xmlFileSuffix
    
    ## API Query Parameters
    ## Append search date to url
    
    
    
    
    ###End Date Time - Filter with a start date before this date
    #endDateTime = datetime.utcnow() + dt.timedelta(hours=-8) #This grabs today's date, then puts an offset of X days (UTC -8:00 for PST)
    #endDateTime = datetime.strftime(endDateTime, '%Y-%m-%dT%H:%M:%SZ')
    #endDateTime = '&EndDateTime=' + str(endDateTime) #End the last time of day to the URL so we include the whole day
    ##endDateTime = '&EndDateTime=2018-02-26T23:59:59Z' ## Tester for picking a specific time
    #
    ### Limit results to not be very far into the past
    #startDateTime = datetime.utcnow() + dt.timedelta(days=-1)
    #startDateTime = datetime.strftime(startDateTime, '%Y-%m-%dT%H:%M:%SZ')
    #startDateTime = '&startDateTime=' + str(startDateTime)
    
    
    
    ## Limit results to not be very far into the past
    startDateTime = datetime.utcnow() + dt.timedelta(hours=-8)
    startDateTime = datetime.strftime(startDateTime, '%Y-%m-%dT08:00:00Z')
    startDateTime = '&startDateTime=' + str(startDateTime)
    
    ##End Date Time - Filter with a start date before this date
    endDateTime = datetime.utcnow() + dt.timedelta(hours=8) #This grabs today's date, then puts an offset of X days (UTC -8:00 for PST)
    endDateTime = datetime.strftime(endDateTime, '%Y-%m-%dT08:00:00Z')
    endDateTime = '&EndDateTime=' + str(endDateTime) #End the last time of day to the URL so we include the whole day
    #endDateTime = '&EndDateTime=2018-02-26T23:59:59Z' ## Tester for picking a specific time
    
    urlBase = urlBase + startDateTime + endDateTime
    print urlBase +'\n'
    
        
    ### XML SECTION
    # Tutorial - http://stackabuse.com/reading-and-writing-xml-files-in-python/
    #ElementTree exports and sorts lexiconically, don't try pointless changing that against W3C standards
    
    #Info portion of the xml, should pull some description that is a tweet 2.0 length or less
    
    #Init the main XML tree thing
    newMarkerData = ET.Element('markers')
    xmlAlreadyExists = os.path.isfile(dir_path + '/' + xmlFilename)
    print "Does this file already exist?" + dir_path + '/' + xmlFilename
    print xmlAlreadyExists
    if xmlAlreadyExists:
        print "XML file already exists!"
        xmlFilenameBackup = xmlFilePrefix + "_" + str(datetime.now(tz=None).strftime("%Y%m%d-%H%M%S")) + xmlFileSuffix
        print xmlFilenameBackup
        
        source_name = os.path.join(dir_path, xmlFilename)
        backup_name = os.path.join(dir_path + '/backup/', xmlFilenameBackup)
        shutil.copy(source_name, backup_name)
        
        os.remove(dir_path + '/' + xmlFilename)
        print "Making a copy of " + xmlFilename + " and storing it in backup folder as " + backup_name
        #tree = ET.parse(xmlFilename) #This is to append a current file, great for re-parsing and appending a current data set
        #print tree
    
    
    
    #Sets up an XML element specific to Layercake's xml structure
    def newLayerMarker(placeId, name, address, lat, lng, markerType, info, source, newMarkerData):
        
        #Inits a new marker line item in the XML data
        item = ET.SubElement(newMarkerData, 'marker') 
        
        #Create each element of the marker
        #item.set('id', markerId) #Ommitted because it's a pointless metric
        item.set('name', name)  
        item.set('address', address)
        item.set('lat', lat)
        item.set('lng', lng)
        item.set('type', markerType)
        item.set('info', info)
        item.set('source', source)
        item.set('id', placeId)
        
        print unidecode(name)
        print unidecode(address)
        print lat + ", " + lng + ", " + placeId
        print unidecode(markerType) 
        #print info
        print unidecode(source)
        print " "
    
           
        return (newMarkerData)
    
    #### Google Geocode API if needed ####
    #def markerGeocode(address):
    #    googleApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json'  
    #
    #    #This passes the 'address' argument through and appends the Google Api key defined at the top
    #    #There's probably some slick params that can be passed through as well
    #    #I tried using the google maps python api client, but it seemed waaaay overkill, see aborted function above
    #    params = {
    #        'address': address,
    #        'key': googleGeocodeKey
    #    }
    #    
    #    #Defines the JSON request and puts the response to a variable
    #    req = requests.get(googleApiUrl, params=params)
    #    res = req.json() #Here's the JSON Response
    #
    #    #Exception handling for Geocode responses. If it doesn't have content, point to Apple HQ
    #    try:
    #        result = res['results'][0] # Hopefully just discard {u'status': u'OK'
    #    
    #    except IndexError: #If you get an indexError (like from a 403 or invalid address, make it point to Apple HQ)
    #        result = {u'geometry': {u'location_type': u'GEOMETRIC_CENTER', u'bounds': {u'northeast': {u'lat': 37.3335129, u'lng': -122.028577}, u'southwest': {u'lat': 37.3305164, u'lng': -122.0321699}}, u'viewport': {u'northeast': {u'lat': 37.3335129, u'lng': -122.028577}, u'southwest': {u'lat': 37.3305164, u'lng': -122.0321699}}, u'location': {u'lat': 37.3320003, u'lng': -122.0307812}}, u'address_components': [{u'long_name': u'Infinite Loop', u'types': [u'route'], u'short_name': u'Infinite Loop'}, {u'long_name': u'Cupertino', u'types': [u'locality', u'political'], u'short_name': u'Cupertino'}, {u'long_name': u'Santa Clara County', u'types': [u'administrative_area_level_2', u'political'], u'short_name': u'Santa Clara County'}, {u'long_name': u'California', u'types': [u'administrative_area_level_1', u'political'], u'short_name': u'CA'}, {u'long_name': u'United States', u'types': [u'country', u'political'], u'short_name': u'US'}, {u'long_name': u'95014', u'types': [u'postal_code'], u'short_name': u'95014'}], u'place_id': u'ChIJ-7m057a1j4AR2VBPVzJDemk', u'formatted_address': u'Infinite Loop, Cupertino, CA 95014, USA', u'types': [u'route']}
    #
    #    
    #    formattedAddress = str(unidecode(result['formatted_address'])) ##This needs to be converted to ASCII strictly because even unicode 8 breaks the str()
    #                                                                    ##UnicodeEncodeError: 'ascii' codec can't encode character u'\u2019' in position 4: ordinal not in range(128)
    #                                                                    ## Apparently google's API will respond with UTF-8 characters potentially  
    #    latLocal = str(result['geometry']['location']['lat'])
    #    lngLocal = str(result['geometry']['location']['lng'])
    #    placeIdLocal = str(result['place_id'])
    #    
    #    #print('{address}. (lat, lng) = ({lat}, {lng})'.format(**geodata))
    #    return (latLocal, lngLocal, placeIdLocal, formattedAddress)
    
    
    
    #This iterates through our values in our current JSON data list    
    def jsonParser(jsonDataItems):
        for value in jsonDataItems:
    #        if value['dates']['status']['code'] != 'offsale': ##This filters any results that are not 'offsale'
    #            print str(value['dates']['status']['code'])
    
            try:
                eventStartTime = str(value['dates']['start']['localDate']) + 'T' + str(value['dates']['start']['localTime'])
                #eventStartTime = eventStartTime[:-1] #To get rid of Z at the end for UTC Zero
                eventStartTime = datetime.strptime(eventStartTime, "%Y-%m-%dT%H:%M:%S")
                eventStartTime = datetime.strftime(eventStartTime, "%-I:%M %p") #Need to add a # symbol in Windows, '-' if Linux to %I so it's %#I or %-I
                eventStartTime = str(eventStartTime)
                try: 
                    eventEndTime = str(value['dates']['end']['localDate']) + 'T' + str(value['dates']['end']['localTime'])
                    eventEndTime = datetime.strptime(eventEndTime, "%Y-%m-%dT%H:%M:%S")
                    eventEndTime = datetime.strftime(eventEndTime, "%-I:%M %p, %a %b %d")
                    eventEndTime = " to " + str(eventEndTime)
                except:
                    eventStartTime = str(value['dates']['start']['localDate']) + 'T' + str(value['dates']['start']['localTime'])
                    #eventStartTime = eventStartTime[:-1] #To get rid of Z at the end for UTC Zero
                    eventStartTime = datetime.strptime(eventStartTime, "%Y-%m-%dT%H:%M:%S")
                    eventStartTime = datetime.strftime(eventStartTime, "%-I:%M %p, %a %b %d")
                    eventStartTime = str(eventStartTime)
                    eventEndTime = ''
                    
    
                name = value['name']+ " (" + eventStartTime + eventEndTime + ")" + " - Status: " + value['dates']['status']['code']
                #print unidecode(name)
            except KeyError:
                try:
                    name = value['name']
                except KeyError:
                    name = "Event"
            
        #    try:
        #        geoDataLocal = markerGeocode(value['venue']['address']['localized_address_display']) #This helps google find locations based on the crap DataSF Locations
        #    except KeyError:
        #        geoDataLocal = ['KeyError','KeyError','KeyError','KeyError']

            #Address assembly
            try:
                address1 = value['_embedded']['venues'][0]['address']['line1']
            except KeyError:
                address1 = ''
            try:
                address2 = ', ' + value['_embedded']['venues'][0]['address']['line2']
            except KeyError:
                address2 = ''
            try:
                address3 = ', ' + value['_embedded']['venues'][0]['address']['line3']
            except KeyError:
                address3 = ''
            try:
                city = ', ' + value['_embedded']['venues'][0]['city']['name'] 
            except KeyError:
                city = ''
            try:
                state = ', ' + value['_embedded']['venues'][0]['state']['name']
            except KeyError:
                state = ''
            try:
                postalCode = ' ' + value['_embedded']['venues'][0]['postalCode']
            except KeyError:
                postalCode = ''
            
            address = address1 + address2 + address3 + city + state + postalCode
            
            
            placeId = value['id']
            lat = value['_embedded']['venues'][0]['location']['latitude']
            lng = value['_embedded']['venues'][0]['location']['longitude']
            markerType = "Ticketmaster_SF"
            
            try:
                infoVenue = value['_embedded']['venues'][0]['name']
                infoVenue = '<b>Venue:</b> ' + infoVenue
            except KeyError:
                infoVenue = ''
            
            try:
                infoBasic = value['info']
            except KeyError:
                infoBasic = ''
            
            try:
                infoPleaseNote = ' ' + value['pleaseNote'] + ' '
            except KeyError:
                infoPleaseNote = ''
            
            try: 
                infoMinPrice = value['priceRanges'][0]['min']
                infoMaxPrice = value['priceRanges'][0]['max']
                
                if infoMinPrice == infoMaxPrice:
                    infoEventPrice = ' (' + '${:,.2f}'.format(infoMinPrice) + ') '
                else:
                    infoEventPrice = ' (' + '${:,.2f}'.format(infoMinPrice) + ' - ' + '${:,.2f}'.format(infoMaxPrice) + ') '       
            except KeyError:
                infoEventPrice = ''
            
            try:
                info = infoVenue + infoEventPrice + '<br>' + infoBasic + infoPleaseNote
                #Trim to extended tweet length and the dots (277) or short (144+3??)
                if info is not None:
                    info = (info[:277] + '...') if len(info) > 277 else info
                else:
                    info = ""
            except KeyError:
                info = ""
            
            
            
            try:
                source = value['url']
            except KeyError:
                source = "http://www.ticketmaster.com"
            
            
            
            xmlDataRaw = newLayerMarker(placeId, name, address, lat, lng, markerType, info, source, newMarkerData)
            
            #Take the new data, toss it into string format
            xmlDataString = ET.tostring(xmlDataRaw)
            
    #        #Make the data legible in the terminal window, this grows exponentially
    #        xmlDataPrint = xml.dom.minidom.parseString(item)
    #        print xmlDataPrint.toprettyxml()
                    
            #Open, write, close the XML file every loop so we get progressive saving
            myfile = open((dir_path + '/' + xmlFilename), "w")  
            myfile.write(xmlDataString)  
            myfile.close()
            
            time.sleep(0.05) #So we don't spam the servers, mostly google
    #        else: ##Else statement that is a part of the status code filter
    #            time.sleep(0.05)
    
    if importFeed:
        jsonData = urlopen(urlBase).read()
        jsonData = json.loads(jsonData)
    else:
        with open('Ticketmaster.html') as f:  
            jsonData = json.load(f)
    
    
     
        
    
     
    print len(jsonData)
    ##Search through the JSON for needed sections
        #This is for data sets with keys, such as the JSON output from Ford GoBike
        #Comment out if the data is already setup in a single, non-nested dict
    print jsonData.keys()
    
    jsonDataList = jsonData.keys()
    #print len(jsonDataList)
    
    #print jsonData.items()
    #print str(unidecode(jsonData['_embedded']['events'][1]['name']))
    jsonDataItems = jsonData['_embedded']['events']
    
    
    print "First Page length of jsonDataItems: " + str(len(jsonDataItems)) + '\n'
    
    
    
    def getNextPage(urlPage):
        
        print "Parsing page url: " + urlPage
        
        jsonData = urlopen(urlPage).read()
        jsonData = json.loads(jsonData)
        
        #print jsonData.items()
        #print jsonData['events'][1]['name']['text']
        try:
            jsonDataItems = jsonData['_embedded']['events']
        except KeyError:
            print "KeyError on converting the JSON results, setting to 'empty'"
            jsonDataItems = []
        return jsonDataItems
    
    
    
    
    ##Main Code Execution Loop
    jsonParser(jsonDataItems)
    getNextPageExists = 1
    pageCounter = 1
    while getNextPageExists:
        urlPage = urlBase + '&page=' + str(pageCounter)
        print "While loop page init: " + urlPage
        print " "
        
        jsonDataItems = getNextPage(urlPage)
        
        print "Length of jsonDataItems: " + str(len(jsonDataItems)) + " on page " + str(pageCounter)
        print " "
        if len(jsonDataItems) > 0:
            pageCounter = pageCounter + 1
            jsonParser(jsonDataItems)
            urlPage = urlBase + '&page=' + str(pageCounter)
            print "Found an additional page of data!"
            
            print "Going to page: " + str(pageCounter)
            getNextPageExists = 1
            
        else:
            getNextPageExists = 0
            print "No more pages"
            
    print "All done"
   
#Must add this to make it externally callable from the master scraper    
if __name__ == '__main__':
    # test1.py executed as script
    # do something
    main_execute()
