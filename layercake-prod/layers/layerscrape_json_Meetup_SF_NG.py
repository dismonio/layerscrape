# -*- coding: utf-8 -*-
"""
Created on Sat Feb 03 18:24:55 2018

@author: Samuel Steele
@email: sam@sam-steele.com
"""

import json #For json parsing
import xml.etree.ElementTree as ET #for XML manipulation
import xml.dom.minidom
from urllib import urlopen #For pulling in URLs from the web
import os #For seeing if files exist
import shutil #For copying files
import requests #For Google Geocode
import time #For waiting between API Calls
from unidecode import unidecode #To clean up strings
import datetime as dt #For converting datetime into something useful (Eventbrite start/end)
from datetime import datetime
import socket #necessary for exception handling of URL gets



def main_execute():
    #Google API keys, these need to be updated when pushed to production
    googleGeocodeKey = 'AIzaSyB8qtqib7zL9Xu4eKAClcXAeWNTTxpREn8'
    googlePlacesKey = 'AIzaSyB_RV9jTPOHd7Xx-KBta-oT0pSrKeNYGJ0'
    
    dir_path = os.path.dirname(os.path.realpath(__file__))
    print "The working directory is: " + dir_path
    
    #Do you want to pull live JSON data?
    importFeed = 1
    
    ############### MANUAL ENTRY REQUIRED HERE##############
    ##key, needs to request sign=true, lat, lon, radius 50mi, page=1000 which actually means give me as close to X results as possible on the page for Meetup's case
    urlBase = 'https://api.meetup.com/find/upcoming_events' #&end_date_range=2018-02-26T23:59:59'
    markerType = "Meetup_SF"
    xmlFilePrefix = "Meetup_SF"
    xmlFileSuffix = ".xml"
    xmlFilename = xmlFilePrefix + xmlFileSuffix

    
    ## API Query Parameters
    ## Append search date to url
    
    ## Limit results to not be very far into the past
    #startDateTime = datetime.utcnow() + dt.timedelta(hours=-8)
    #startDateTime = datetime.strftime(startDateTime, '%Y-%m-%dT00:00:00')
    #startDateTime = str(startDateTime)
    startDateTime = '' #No start date time is needed to be included in the query (the Meetup API doesn't seem to respond to this)
    
    ##End Date Time - Filter with a start date before this date
    endDateTime = datetime.utcnow() + dt.timedelta(hours=-8) #This grabs today's date, then puts an offset of X days (UTC -8:00 for PST)
    endDateTime = datetime.strftime(endDateTime, '%Y-%m-%dT23:59:59')
    endDateTime = str(endDateTime) #End the last time of day to the URL so we include the whole day
    #endDateTime = '&EndDateTime=2018-02-26T23:59:59Z' ## Tester for picking a specific time
    

    
        
    ### XML SECTION
    # Tutorial - http://stackabuse.com/reading-and-writing-xml-files-in-python/
    #ElementTree exports and sorts lexiconically, don't try pointless changing that against W3C standards
    
    #Info portion of the xml, should pull some description that is a tweet 2.0 length or less
    
    #Init the main XML tree thing
    newMarkerData = ET.Element('markers')
    xmlAlreadyExists = os.path.isfile(dir_path + '/' + xmlFilename)
    print "Does this file already exist?" + dir_path + '/' + xmlFilename
    print xmlAlreadyExists
    if xmlAlreadyExists:
        print "XML file already exists!"
        xmlFilenameBackup = xmlFilePrefix + "_" + str(datetime.utcnow().strftime("%Y%m%d-%H%M%S")) + xmlFileSuffix
        print xmlFilenameBackup
        
        source_name = os.path.join(dir_path, xmlFilename)
        backup_name = os.path.join(dir_path + '/backup/', xmlFilenameBackup)
        shutil.copy(source_name, backup_name)
        
        print "Making a copy of " + xmlFilename + " and storing it in backup folder as " + backup_name
        #tree = ET.parse(xmlFilename) #This is to append a current file, great for re-parsing and appending a current data set
        #print tree
    if not xmlAlreadyExists:
        xmlFile = open((dir_path + '/' + xmlFilename), "w")  #Make the new temp file to store everything in memory
        xmlFile.close() #Close the temp file
    
    
    
    #Sets up an XML element specific to Layercake's xml structure
    def newLayerMarker(placeId, name, address, lat, lng, markerType, info, source, newMarkerData):
        
        #Inits a new marker line item in the XML data
        item = ET.SubElement(newMarkerData, 'marker') 
        
        #Create each element of the marker
        #item.set('id', markerId) #Ommitted because it's a pointless metric
        item.set('name', name)  
        item.set('address', address)
        item.set('lat', lat)
        item.set('lng', lng)
        item.set('type', markerType)
        item.set('info', info)
        item.set('source', source)
        item.set('id', placeId)
        
        print unidecode(name)
        print unidecode(address)
        print lat + ", " + lng + ", " + placeId
        print unidecode(markerType) 
        #print info
        print unidecode(source)
        print " "
    
           
        return (newMarkerData)
    
    #### Google Geocode API if needed ####
    #def markerGeocode(address):
    #    googleApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json'  
    #
    #    #This passes the 'address' argument through and appends the Google Api key defined at the top
    #    #There's probably some slick params that can be passed through as well
    #    #I tried using the google maps python api client, but it seemed waaaay overkill, see aborted function above
    #    params = {
    #        'address': address,
    #        'key': googleGeocodeKey
    #    }
    #    
    #    #Defines the JSON request and puts the response to a variable
    #    req = requests.get(googleApiUrl, params=params)
    #    res = req.json() #Here's the JSON Response
    #
    #    #Exception handling for Geocode responses. If it doesn't have content, point to Apple HQ
    #    try:
    #        result = res['results'][0] # Hopefully just discard {u'status': u'OK'
    #    
    #    except IndexError: #If you get an indexError (like from a 403 or invalid address, make it point to Apple HQ)
    #        result = {u'geometry': {u'location_type': u'GEOMETRIC_CENTER', u'bounds': {u'northeast': {u'lat': 37.3335129, u'lng': -122.028577}, u'southwest': {u'lat': 37.3305164, u'lng': -122.0321699}}, u'viewport': {u'northeast': {u'lat': 37.3335129, u'lng': -122.028577}, u'southwest': {u'lat': 37.3305164, u'lng': -122.0321699}}, u'location': {u'lat': 37.3320003, u'lng': -122.0307812}}, u'address_components': [{u'long_name': u'Infinite Loop', u'types': [u'route'], u'short_name': u'Infinite Loop'}, {u'long_name': u'Cupertino', u'types': [u'locality', u'political'], u'short_name': u'Cupertino'}, {u'long_name': u'Santa Clara County', u'types': [u'administrative_area_level_2', u'political'], u'short_name': u'Santa Clara County'}, {u'long_name': u'California', u'types': [u'administrative_area_level_1', u'political'], u'short_name': u'CA'}, {u'long_name': u'United States', u'types': [u'country', u'political'], u'short_name': u'US'}, {u'long_name': u'95014', u'types': [u'postal_code'], u'short_name': u'95014'}], u'place_id': u'ChIJ-7m057a1j4AR2VBPVzJDemk', u'formatted_address': u'Infinite Loop, Cupertino, CA 95014, USA', u'types': [u'route']}
    #
    #    
    #    formattedAddress = str(unidecode(result['formatted_address'])) ##This needs to be converted to ASCII strictly because even unicode 8 breaks the str()
    #                                                                    ##UnicodeEncodeError: 'ascii' codec can't encode character u'\u2019' in position 4: ordinal not in range(128)
    #                                                                    ## Apparently google's API will respond with UTF-8 characters potentially  
    #    latLocal = str(result['geometry']['location']['lat'])
    #    lngLocal = str(result['geometry']['location']['lng'])
    #    placeIdLocal = str(result['place_id'])
    #    
    #    #print('{address}. (lat, lng) = ({lat}, {lng})'.format(**geodata))
    #    time.sleep(0.05) #So we don't spam the servers, mostly google
    #    return (latLocal, lngLocal, placeIdLocal, formattedAddress)
    
    
    
    #This iterates through our values in our current JSON data list    
    def jsonParser(jsonDataItems):
        for value in jsonDataItems:
            try:
                value['venue'] ##This filters any results that are not 'offsale'
                validDateTime = datetime.utcnow() + dt.timedelta(hours=-8)
                validDateTime = datetime.strftime(validDateTime, '%Y-%m-%d')
                
                validTomorrowDateTime = datetime.utcnow() + dt.timedelta(hours=16) #24utc tmrw - 8 tzoffset = 16
                validTomorrowDateTime = datetime.strftime(validTomorrowDateTime, '%Y-%m-%d')
                #print str(value['local_date']) + ' and ' + str(validDateTime)
                
                if (str(value['local_date']) == str(validDateTime)) or (str(value['local_date']) == str(validTomorrowDateTime)):
                    
                    #Try to execute the loop for linux time stamp format
                    try: 
                        eventStartTime = str(value['local_date']) + 'T' + str(value['local_time'])
                        #eventStartTime = eventStartTime[:-1] #To get rid of Z at the end for UTC Zero
                        eventStartTime = datetime.strptime(eventStartTime, "%Y-%m-%dT%H:%M")
                        eventStartTime = datetime.strftime(eventStartTime, "%-I:%M %p") #Need to add a # symbol in Windows, '-' if Linux to %I so it's %#I or %-I
                        eventStartTime = str(eventStartTime)
                        try: 
                            eventEndTime = str(value['dates']['end']['local_date']) + 'T' + str(value['dates']['end']['local_time'])
                            eventEndTime = datetime.strptime(eventEndTime, "%Y-%m-%dT%H:%M")
                            eventEndTime = datetime.strftime(eventEndTime, "%-I:%M %p, %a %b %d")
                            eventEndTime = " to " + str(eventEndTime)
                        except:
                            eventStartTime = str(value['local_date']) + 'T' + str(value['local_time'])
                            #eventStartTime = eventStartTime[:-1] #To get rid of Z at the end for UTC Zero
                            eventStartTime = datetime.strptime(eventStartTime, "%Y-%m-%dT%H:%M")
                            eventStartTime = datetime.strftime(eventStartTime, "%-I:%M %p, %a %b %d")
                            eventStartTime = str(eventStartTime)
                            eventEndTime = ''
                            
            
                        name = value['name']+ " (" + eventStartTime + eventEndTime + ")" + " - Status: " + value['status']
                        #print unidecode(name)
                    
                    
                    #If Linux timestamp didn't work, try the Windows version
                    #Need to add a # symbol in Windows, '-' if Linux to %I so it's %#I or %-I    
                    except ValueError as errorMessage: 
                        if str(errorMessage) != 'Invalid format string':
                            print 'Invalid format string in time calculation of jsonParser'
                        else:
                            eventStartTime = str(value['local_date']) + 'T' + str(value['local_time'])
                            #eventStartTime = eventStartTime[:-1] #To get rid of Z at the end for UTC Zero
                            eventStartTime = datetime.strptime(eventStartTime, "%Y-%m-%dT%H:%M")
                            eventStartTime = datetime.strftime(eventStartTime, "%#I:%M %p") #Need to add a # symbol in Windows, '-' if Linux to %I so it's %#I or %-I
                            eventStartTime = str(eventStartTime)
                            try: 
                                eventEndTime = str(value['dates']['end']['local_date']) + 'T' + str(value['dates']['end']['local_time'])
                                eventEndTime = datetime.strptime(eventEndTime, "%Y-%m-%dT%H:%M")
                                eventEndTime = datetime.strftime(eventEndTime, "%#I:%M %p, %a %b %d")
                                eventEndTime = " to " + str(eventEndTime)
                            except:
                                eventStartTime = str(value['local_date']) + 'T' + str(value['local_time'])
                                #eventStartTime = eventStartTime[:-1] #To get rid of Z at the end for UTC Zero
                                eventStartTime = datetime.strptime(eventStartTime, "%Y-%m-%dT%H:%M")
                                eventStartTime = datetime.strftime(eventStartTime, "%#I:%M %p, %a %b %d")
                                eventStartTime = str(eventStartTime)
                                eventEndTime = ''
                                
                
                            name = value['name']+ " (" + eventStartTime + eventEndTime + ")" + " - Status: " + value['status']
                            #print unidecode(name)
                        
                    except KeyError:
                        try:
                            name = value['name']
                        except KeyError:
                            name = "Event"
                    
                #    try:
                #        geoDataLocal = markerGeocode(value['venue']['address']['localized_address_display']) #This helps google find locations based on the crap DataSF Locations
                #    except KeyError:
                #        geoDataLocal = ['KeyError','KeyError','KeyError','KeyError']
                    try:
                        address1 = value['venue']['address_1']
                    except KeyError:
                        address1 = ''
                    try:
                        address2 = ', ' + value['venue']['address_2']
                    except KeyError:
                        address2 = ''
                    try:
                        address3 = ', ' + value['venue']['address_2']
                    except KeyError:
                        address3 = ''
                    try:
                        city = ', ' + value['venue']['city'] 
                    except KeyError:
                        city = ''
                    try:
                        state = ', ' + value['venue']['state']
                    except KeyError:
                        state = ''
                    try:
                        postalCode = ' ' + value['venue']['zip']
                    except KeyError:
                        postalCode = ''
                    address = address1 + address2 + address3 + city + state + postalCode
                    placeId = str(value['id'])
                    lat = str(value['venue']['lat'])
                    lng = str(value['venue']['lon'])
                    markerType = "Meetup_SF"
                    try:
                        infoVenue = value['venue']['name']
                        infoVenue = '<b>Venue:</b> ' + infoVenue
                    except KeyError:
                        infoVenue = ''
                    
                    try:
                        infoBasic = value['description']
                    except KeyError:
                        infoBasic = ''
                    
                    try:
                        infoPleaseNote = ' ' + value['how_to_find_us'] + ' '
                    except KeyError:
                        infoPleaseNote = ''
                    
                    try: 
                        infoMinPrice = value['fee']['amount']
                        infoMaxPrice = value['priceRanges'][0]['max']
                        
                        infoEventPrice = ' (' + '${:,.2f}'.format(infoMinPrice) + ') '
                  
                    except KeyError:
                        infoEventPrice = ''
                    
                    try:
                        info = infoVenue + infoEventPrice + '<br>' + infoBasic + infoPleaseNote
                        #Trim to extended tweet length and the dots (277) or short (144+3??)
                        if info is not None:
                            info = (info[:277] + '...') if len(info) > 277 else info
                        else:
                            info = ""
                    except KeyError:
                        info = ""
                    
                    
                    
                    try:
                        source = value['link']
                    except KeyError:
                        source = "http://www.meetup.com"
                    
                    
                    
                    xmlDataRaw = newLayerMarker(placeId, name, address, lat, lng, markerType, info, source, newMarkerData)
                    
                    #Take the new data, toss it into string format
                    xmlDataString = ET.tostring(xmlDataRaw)
#                    print xmlDataRaw
                    
#                    #Make the data legible in the terminal window, this grows exponentially, used for verifying XML real output
#                    xmlDataPrint = xml.dom.minidom.parseString(xmlDataString)
#                    print xmlDataPrint.toprettyxml()
                    
            
                            
                    
                else:
                    time.sleep(0.05)
            except KeyError: ##Else statement that is a part of the status code filter
                pass
                #print 'Generic Venue KeyError for inside jsonParser function' + '\n' #Leave this off unless you want a chatty Meetup Parser
        return xmlDataString
    
    if importFeed:
        
        urlPayload = {
            'key' : '79182d4950193f3b2b3415a4265d70',
            'sign': 'true',
            'lon' : '-122.419416',
            'lat' : '37.782000',
            'radius' :'50',
            #'startDateTime' : startDateTime,
            'EndDateTime' : endDateTime,
            'page' : '1000'
            }
        urlAuth = {
                'key' : '79182d4950193f3b2b3415a4265d70',
                }

    
        #httpData = urlopen(urlBase)  #this uses urllib
        httpData = requests.get(urlBase, params=urlPayload) #this uses requests library
        print str(httpData.url) + '\n'
        
        #print httpData.status_code()
        #print "httpStatusCode: "
        #print str(httpStatusCode) + "\n"
        
        httpHeaderLink = httpData.headers.get('link', None)
        if httpHeaderLink is not None:
            httpHeaderLink = httpData.links["next"]['url']
            print "httpHeaderLink: "
            print httpHeaderLink
            getNextPageExists = 1
        
        jsonData = httpData.json()
        #jsonData = json.loads(jsonData) #required for urllib
    else:
        with open('Meetup.html') as f:  
            jsonData = json.load(f)
    
    
     
        
    
     
    print len(jsonData)
    ##Search through the JSON for needed sections
        #This is for data sets with keys, such as the JSON output from Ford GoBike
        #Comment out if the data is already setup in a single, non-nested dict
    #print jsonData.keys()
    #print type(jsonData)
    
    #jsonDataList = jsonData.keys()
    #print len(jsonDataList)
    
    #print jsonData.items()
    #print str(unidecode(jsonData['_embedded']['events'][1]['name']))
    jsonDataItems = jsonData['events']
    #print jsonDataItems
    
    print "First Page length of jsonDataItems: " + str(len(jsonDataItems)) + '\n'
    
    
    
    def getNextPage(urlPage):

        try:
            httpData = requests.get(urlPage, params=urlAuth)
            jsonData = httpData.json()
        except socket.error as errorMessage:
            time.sleep(5) #Wait 5 seconds to re-try the connection
            httpData = requests.get(urlPage, params=urlAuth)
            jsonData = httpData.json()
            print str(errorMessage)
        except:
            print "Unexpected Error, check /var/speed/mail/ec2-user log"
            
        
        print "Parsing page url: " + str(httpData.url) + '\n'
        
        if httpHeaderLink is not None:
            getNextPageExists = 1
        if httpHeaderLink is None:
            getNextPageExists = 0
        
        #print jsonData.items()
        #print jsonData['events'][1]['name']['text']
        try:
            jsonDataItems = jsonData['events']
        except KeyError:
            print "KeyError on converting the JSON results, setting to 'empty'"  
            jsonDataItems = []
        return jsonDataItems, httpData
    
    
    def xmlFileMaker(xmlDataString):
        try:
            #Take the time now to make the new file as a temp one
            print "Making the temporary xml file \n"
            #print xmlDataString #Use this to test the XML content while it's still in memory before file writing
            print dir_path + '/' + xmlFilename + '.temp' + '\n'
            
            xmlFile = open((dir_path + '/' + xmlFilename + '.temp'), "w")  #Make the new temp file to store everything in memory
            xmlFile.write(xmlDataString)
            xmlFile.close() #Close the temp file
            
            #Delete the old file and immediately rename the new file by removing the .temp extension
            print "Removing the primary file and repacing it with the temporary file - " + xmlFilename
            os.remove(dir_path + '/' + xmlFilename) #Remove the main file
            os.rename((dir_path + '/' + xmlFilename + '.temp'), (dir_path + '/' + xmlFilename)) #Replace with the temporary file by renaming
            
            
        except:
            print "Unexpected error with xmlFileMaker, can't raise due to lack of error handling"
            raise
            

    
    
    
    ##Main Code Execution Loop
    xmlDataString = jsonParser(jsonDataItems)
    #getNextPageExists = 0 ##There is no next page exists for Meetup, results on one page --This is a huge lie, it's in the HTTP header
    pageCounter = 1
    while getNextPageExists:
        #urlPage = urlBase + '&page=' + str(pageCounter)
        print "While loop page init: " + str(httpHeaderLink) + " \n"
        
        getNextPageData = getNextPage(httpHeaderLink)
        jsonDataItems = getNextPageData[0]
        httpData = getNextPageData[1]
        
        print "Length of jsonDataItems: " + str(len(jsonDataItems)) + " on page " + str(pageCounter) + "\n"
        
        try:
            httpHeaderLink = httpData.links["next"]['url']
    
            if httpHeaderLink is not None:
                pageCounter = pageCounter + 1
                xmlDataString = jsonParser(jsonDataItems)
                httpHeaderLink = httpData.links["next"]['url']
                print "Found an additional page of data!"
                
                print "Going to page: " + str(httpHeaderLink) + "\n"
                getNextPageExists = 1
                time.sleep(0.05) #So we don't spam the servers, mostly google
                
            else:
                getNextPageExists = 0
                print "No more pages"
                
        except KeyError, errorMessage:
            getNextPageExists = 0
            print "No more pages, this is okay, end of pagination, KeyError '%s'." % str(errorMessage)
            
#            try:
            
            #Commit the XML we've built to a file before shutting down the script
            xmlFileMaker(xmlDataString)
#            except:
#                print "Issue with xmlFileMaker"
        except:
            print "Unexpected Error, check /var/speed/mail/ec2-user log"
            
    print "All done"
   
    
#Must add this to make it externally callable from the master scraper    
if __name__ == '__main__':
    # test1.py executed as script
    # do something
    main_execute()
