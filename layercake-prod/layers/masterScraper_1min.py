# -*- coding: utf-8 -*-
"""
Created on Wed Mar 07 22:44:30 2018

@author: Samuel Steele
@email: sam@sam-steele.com
"""

##Python Master Scraper
##Call this with crontab on the server or whatever you want
##Then use this to manage which scripts are updated

import layerscrape_json_Scoot_Scooters_SF_NG



def scoot_Scooters_SF_NG():
    print "Running layerscrape_json_Scoot_Scooters_SF_NG script"
    print layerscrape_json_Scoot_Scooters_SF_NG.main_execute()
    
scoot_Scooters_SF_NG()

print "All 1 minute dynamic xml files updated (hopefully)"