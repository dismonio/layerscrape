
<!DOCTYPE html>
<html>
	<head>
		<title>Layercake</title>
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<style>
			/* Always set the map height explicitly to define the size of the div
			* element that contains the map. */
			
			#logo {
			margin: auto;
			vertical-align: middle;
			height: 2em;
			}
			.helper {
			display: inline-block;
			vertical-align: middle;
			height: 100%
			}
			#header {
			height: 3.4em;
			text-align: center;
			white-space: nowrap;
			background-color: black;
			}
			#map {
			height: calc(100% - 7.7em);
			}
			/* Optional: Makes the sample page fill the window. */
			html, body {
			height: 100%;
			margin: 0;
			padding: 0;
			}
			#navigation {
			height: 4.3em;
			text-align: center;
			white-space: nowrap;
			background-color: #f7f7f7;
			}
			.button {
			margin: auto;
			vertical-align: middle;
			height: 100%;
			}
			
			
			
			.switch {
			  position: relative;
			  display: inline-block;
			  width: 42px;
			  height: 25px;
			}

			.switch input {display:none;}

			.slider {
			  position: absolute;
			  cursor: pointer;
			  top: 0;
			  left: 0;
			  right: 0;
			  bottom: 0;
			  background-color: #ccc;
			  -webkit-transition: .4s;
			  transition: .4s;
			}

			.slider:before {
			  position: absolute;
			  content: "";
			  height: 21px;
			  width: 21px;
			  left: 1px;
			  bottom: 2px;
			  background-color: white;
			  -webkit-transition: .4s;
			  transition: .4s;
			}

			input:checked + .slider {
			  background-color: #2196F3;
			}

			input:focus + .slider {
			  box-shadow: 0 0 1px #2196F3;
			}

			input:checked + .slider:before {
			  -webkit-transform: translateX(18px);
			  -ms-transform: translateX(18px);
			  transform: translateX(18px);
			}

			/* Rounded sliders */
			.slider.round {
			  border-radius: 24px;
			}

			.slider.round:before {
			  border-radius: 50%;
			}
			
		</style>
	</head>
	<body>
		<div id="header"><span class="helper"></span><img src="logo_white.png" alt="Layercake" id="logo"></div>
		<div id="map"></div>
		
		<label class="switch">
		  <input type="checkbox" onclick="layerGroups.set('BringFido_SF', (this.checked)?map:null);" checked>BringFido_SF
		  <span class="slider round"></span>
		</label>
		<label class="switch">
		  <input type="checkbox" onclick="layerGroups.set('Diners_Drive_ins_and_Dives_SF', (this.checked)?map:null);" checked>Diners_Drive_ins_and_Dives_SF
		  <span class="slider round"></span>
		</label>
		
		<div id="navigation"><span class="helper"></span><img src="btnLayers.png" alt="Layers" class="button"></div>
		<script>
			
			// Note: This example requires that you consent to location sharing when
			// prompted by your browser. If you see the error "The Geolocation service
			// failed.", it means you probably did not give permission for the browser to
			// locate you.
			var map, infoWindow, layerGroups;
			
			
			
			function initMap() {
				map = new google.maps.Map(document.getElementById('map'), {
					center: {lat: 37.782000, lng: -122.419416},
					zoom: 13
				});
				infoWindow = new google.maps.InfoWindow;
				
				layerGroups = new google.maps.MVCObject();
				
				var layers = ["Anthony_Bourdain_The_Layover_S1E5", 
				"Anthony_Bourdain_Parts_Unknown_S6E4", "Anthony_Bourdain_No_Reservations_S5E15", 
				"Diners_Drive_ins_and_Dives_SF", "BringFido_SF"];
				
				function downloadUrl(url, callback) {
					var request = window.ActiveXObject ?
					new ActiveXObject('Microsoft.XMLHTTP') :
					new XMLHttpRequest;
					
					request.onreadystatechange = function() {
						if (request.readyState == 4) {
							callback(request, request.status);
						}
					};
					
					request.open('GET', url, true);
					request.send(null);
				}
				

				for (var i = 0, len = layers.length; i < len; i++) {
					//console.log(i +' /layers/'+layers[i]+'.xml');
					downloadUrl('./layers/'+layers[i]+'.xml', function(data) {  //First anonymous function
						//console.log('inside'+i +' /layers/'+layers[i]+'.xml');
						var xml = data.responseXML;
						var markers = xml.documentElement.getElementsByTagName('marker');

						addMarkersToMap(markers, this.i);
						
					}.bind({i: i}));
					layerGroups.set(layers[i],map);
				}
								
				//for (var i = 0, len = layers.length; i < len; i++) {
				//	console.log(i +' /layers/'+layers[i]+'.xml');
				//}

				//function removeMarkersFromMap(markers) {
				//	Array.prototype.forEach.call(markers, function(markerElem) {
				//		markers.setMap(null);
				//	});
				//}
				
				function addMarkersToMap(markers, layerNumber) { 
					Array.prototype.forEach.call(markers, function(markerElem) { //Second anonymous function
						//Set InfoWindow variables
						
						var name = markerElem.getAttribute('name');
						var address = markerElem.getAttribute('address');
						var type = markerElem.getAttribute('type');
						var point = new google.maps.LatLng(parseFloat(markerElem.getAttribute('lat')), parseFloat(markerElem.getAttribute('lng')));
						var source = markerElem.getAttribute('source');
						var infowincontent = document.createElement('div');
						
						//Add title to InfoWindow
						var strong = document.createElement('strong');
						strong.textContent = name
						infowincontent.appendChild(strong);
						infowincontent.appendChild(document.createElement('br'));
						
						//Add address to InfoWindow
						var addresstext = document.createElement('text');
						addresstext.textContent = address
						infowincontent.appendChild(addresstext);
						infowincontent.appendChild(document.createElement('br'));
						
						//Add Google Maps link to InfoWindow
						var gMapsLink = document.createElement('a');
						gMapsLink.setAttribute('href','https://www.google.com/maps/place/'+encodeURIComponent(address));
						gMapsLink.setAttribute('target', '_blank'); 
						gMapsLink.innerText = 'View on Google Maps';
						infowincontent.appendChild(gMapsLink);
						infowincontent.appendChild(document.createElement('br'));
						
						//Add Source link to InfoWindow
						if (source) { // Skip if source is blank or null
							var sourcetext = document.createElement('text');
							var sourcelink = document.createElement('a');
							sourcetext.textContent = "Source: "
							infowincontent.appendChild(document.createElement('br'));
							sourcelink.setAttribute('href', source);
							sourcelink.setAttribute('target', '_blank'); 
							sourcelink.innerText = source;
							infowincontent.appendChild(sourcetext);
							infowincontent.appendChild(document.createElement('br'));
							infowincontent.appendChild(sourcelink);
						}
						
						//Create marker
						var marker = new google.maps.Marker({
							map: map,
							position: point,
							title: type,
							icon: './layers/'+type+'.png'
						});
						
						//Pop up InfoWindow when marker clicked
						marker.addListener('click', function() { //Third anonymous function
							infoWindow.setContent(infowincontent);
							infoWindow.open(map, marker);
						});
						
						marker.bindTo('map', layerGroups, type);
						//console.log('layerGroups ' + Object.keys(layerGroups));
					});
				}
				

				
				
				// Try HTML5 geolocation.
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function(position) {
						var pos = {
							lat: position.coords.latitude,
							lng: position.coords.longitude
						};
						
						//infoWindow.setPosition(pos);
						//infoWindow.setContent('Location found.');
						//infoWindow.open(map);
						
						var marker = new google.maps.Marker({
							position: {lat: 37.782000, lng: -122.419416}, //position: pos,
							map: map,
							icon: 'bluedot.png'
						});
						//map.setCenter(pos); Re-enable this when HTTPS
						}, function() {
						//handleLocationError(true, infoWindow, map.getCenter());
						var marker = new google.maps.Marker({
							position: {lat: 37.782000, lng: -122.419416},
							map: map,
							icon: 'bluedot.png'
						});
					});
					} else {
					// Browser doesn't support Geolocation
					//handleLocationError(false, infoWindow, map.getCenter());
				}
			}
			
			function handleLocationError(browserHasGeolocation, infoWindow, pos) {
				infoWindow.setPosition(pos);
				infoWindow.setContent(browserHasGeolocation ?
				'Error: The Geolocation service failed.' :
				'Error: Your browser doesn\'t support geolocation.');
				infoWindow.open(map);
			}
		</script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDD4RNMqrsAL9d6M1YgNx0xwFNNiggBhIk&callback=initMap">
		</script>
	</body>
</html>										