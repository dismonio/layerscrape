# -*- coding: utf-8 -*-
"""
Created on Wed Mar 07 22:44:30 2018

@author: Samuel Steele
@email: sam@sam-steele.com
"""

##Python Master Scraper
##Call this with crontab on the server or whatever you want
##Then use this to manage which scripts are updated

import layerscrape_json_Meetup_SF_NG
import layerscrape_json_Ticketmaster_SF_NG
import layerscrape_json_Eventbrite_SF_NG


def meetup_SF_NG():
    print "Running layerscrape_json_Meetup_SF_NG.py script"
    print layerscrape_json_Meetup_SF_NG.main_execute()
    
def ticketmaster_SF_NG():
    print "Running layerscrape_json_Ticketmaster_SF_NG.py script"
    print layerscrape_json_Ticketmaster_SF_NG.main_execute()
    
def eventbrite_SF_NG():
    print "Running layerscrape_json_Eventbrite_SF_NG.py script"
    print layerscrape_json_Eventbrite_SF_NG.main_execute()
    
meetup_SF_NG()
ticketmaster_SF_NG()
eventbrite_SF_NG()
print "All 1 hour dynamic xml files updated (hopefully)"