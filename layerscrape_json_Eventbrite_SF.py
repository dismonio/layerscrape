# -*- coding: utf-8 -*-
"""
Created on Sat Feb 03 18:24:55 2018

@author: Samuel Steele
@email: sam@sam-steele.com
"""

import json #For json parsing
import xml.etree.ElementTree as ET #for XML manipulation
import xml.dom.minidom
from urllib import urlopen #For pulling in URLs from the web
import os #For seeing if files exist
import shutil #For copying files
import requests #For Google Geocode
import time #For waiting between API Calls
from unidecode import unidecode #To clean up strings
from datetime import datetime #For converting datetime into something useful (Eventbrite start/end)

dir_path = os.path.dirname(os.path.realpath(__file__))
print dir_path

#import simplejson

importFeed = 1
url = 'https://www.eventbriteapi.com/v3/events/search/?token=O7GPTT34BIME7D4MMPV2&location.latitude=37.782000&location.longitude=-122.419416&location.within=10mi&start_date.keyword=today&expand=organizer,venue'
markerType = "SF_Film_Locations"

if importFeed:
    jsonData = urlopen(url).read()
    jsonData = json.loads(jsonData)
else:
    with open('Eventbrite.html') as f:  
        jsonData = json.load(f)
 
print len(jsonData)
xmlFilePrefix = "Eventbrite_SF"
xmlFileSuffix = ".xml"
xmlFilename = xmlFilePrefix + xmlFileSuffix

#Google API keys, these need to be updated when pushed to production
googleGeocodeKey = 'AIzaSyB8qtqib7zL9Xu4eKAClcXAeWNTTxpREn8'
googlePlacesKey = 'AIzaSyB_RV9jTPOHd7Xx-KBta-oT0pSrKeNYGJ0'
    
### XML SECTION
# Tutorial - http://stackabuse.com/reading-and-writing-xml-files-in-python/
#ElementTree exports and sorts lexiconically, don't try pointless changing that against W3C standards

#Info portion of the xml, should pull some description that is a tweet 2.0 length or less

#Init the main XML tree thing
newMarkerData = ET.Element('markers')
xmlAlreadyExists = os.path.isfile(xmlFilename)
if xmlAlreadyExists:
    print "XML file already exists!"
    xmlFilenameBackup = xmlFilePrefix + "_" + str(datetime.now(tz=None).strftime("%Y%m%d-%H%M%S")) + xmlFileSuffix
    print xmlFilenameBackup
    
    source_name = os.path.join(dir_path, xmlFilename)
    backup_name = os.path.join(dir_path + '/backup/', xmlFilenameBackup)
    shutil.copy(source_name, backup_name)
    
    os.remove(xmlFilename)
    print "Making a copy of " + xmlFilename + " and storing it in backup folder as " + backup_name
    #tree = ET.parse(xmlFilename) #This is to append a current file, great for re-parsing and appending a current data set
    #print tree


##Search through the JSON for needed sections
    #This is for data sets with keys, such as the JSON output from Ford GoBike
    #Comment out if the data is already setup in a single, non-nested dict
print jsonData.keys()

jsonDataList = jsonData.keys()
print len(jsonDataList)

#print jsonData.items()
print jsonData['events'][1]['name']['text']
jsonDataItems = jsonData['events']

#print len(jsonDataItems)
#print jsonDataItems
    
#print len(jsonData)
#print type(jsonData)
#print json.dumps(jsonData, indent=4, sort_keys=True)


##Since DataSF doesn't have perfect data, some rows are just flat out missing locations
##We need to just delete this data because there's no point in utilizing it without a location
#for index, value in enumerate(jsonData):
#    print(index, value)
#    
#    try:
#        print value['events'][1]['name']['text']
#        print value
#    except KeyError:
#        print "Found one at index: " + str(index)
#        del jsonData[index]
#
#        
#print "Results: "
#print len(jsonData)        
##print json.dumps(jsonData, indent=4, sort_keys=True)


#Sets up an XML element specific to Layercake's xml structure
def newLayerMarker(placeId, name, address, lat, lng, markerType, info, source, newMarkerData):
    
    #Inits a new marker line item in the XML data
    item = ET.SubElement(newMarkerData, 'marker') 
    
    #Create each element of the marker
    #item.set('id', markerId) #Ommitted because it's a pointless metric
    item.set('name', name)  
    item.set('address', address)
    item.set('lat', lat)
    item.set('lng', lng)
    item.set('type', markerType)
    item.set('info', info)
    item.set('source', source)
    item.set('id', placeId)
       
    return (newMarkerData)

def markerGeocode(address):
    googleApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json'  

    #This passes the 'address' argument through and appends the Google Api key defined at the top
    #There's probably some slick params that can be passed through as well
    #I tried using the google maps python api client, but it seemed waaaay overkill, see aborted function above
    params = {
        'address': address,
        'key': googleGeocodeKey
    }
    
    #Defines the JSON request and puts the response to a variable
    req = requests.get(googleApiUrl, params=params)
    res = req.json() #Here's the JSON Response

    #Exception handling for Geocode responses. If it doesn't have content, point to Apple HQ
    try:
        result = res['results'][0] # Hopefully just discard {u'status': u'OK'
    
    except IndexError: #If you get an indexError (like from a 403 or invalid address, make it point to Apple HQ)
        result = {u'geometry': {u'location_type': u'GEOMETRIC_CENTER', u'bounds': {u'northeast': {u'lat': 37.3335129, u'lng': -122.028577}, u'southwest': {u'lat': 37.3305164, u'lng': -122.0321699}}, u'viewport': {u'northeast': {u'lat': 37.3335129, u'lng': -122.028577}, u'southwest': {u'lat': 37.3305164, u'lng': -122.0321699}}, u'location': {u'lat': 37.3320003, u'lng': -122.0307812}}, u'address_components': [{u'long_name': u'Infinite Loop', u'types': [u'route'], u'short_name': u'Infinite Loop'}, {u'long_name': u'Cupertino', u'types': [u'locality', u'political'], u'short_name': u'Cupertino'}, {u'long_name': u'Santa Clara County', u'types': [u'administrative_area_level_2', u'political'], u'short_name': u'Santa Clara County'}, {u'long_name': u'California', u'types': [u'administrative_area_level_1', u'political'], u'short_name': u'CA'}, {u'long_name': u'United States', u'types': [u'country', u'political'], u'short_name': u'US'}, {u'long_name': u'95014', u'types': [u'postal_code'], u'short_name': u'95014'}], u'place_id': u'ChIJ-7m057a1j4AR2VBPVzJDemk', u'formatted_address': u'Infinite Loop, Cupertino, CA 95014, USA', u'types': [u'route']}

    
    formattedAddress = str(unidecode(result['formatted_address'])) ##This needs to be converted to ASCII strictly because even unicode 8 breaks the str()
                                                                    ##UnicodeEncodeError: 'ascii' codec can't encode character u'\u2019' in position 4: ordinal not in range(128)
                                                                    ## Apparently google's API will respond with UTF-8 characters potentially  
    latLocal = str(result['geometry']['location']['lat'])
    lngLocal = str(result['geometry']['location']['lng'])
    placeIdLocal = str(result['place_id'])
    
    #print('{address}. (lat, lng) = ({lat}, {lng})'.format(**geodata))
    return (latLocal, lngLocal, placeIdLocal, formattedAddress)

for value in jsonDataItems:
    try:
        eventbriteStartTime = str(value['start']['local'])
        eventbriteStartTime = datetime.strptime(eventbriteStartTime, "%Y-%m-%dT%H:%M:%S")
        eventbriteStartTime = datetime.strftime(eventbriteStartTime, "%I:%M %p")

        eventbriteEndTime = str(value['end']['local'])
        eventbriteEndTime = datetime.strptime(eventbriteEndTime, "%Y-%m-%dT%H:%M:%S")
        eventbriteEndTime = datetime.strftime(eventbriteEndTime, "%I:%M %p, %a %b %d")


        name = value['name']['text']+ " (" + str(eventbriteStartTime) + " to " + str(eventbriteEndTime) + ")"
        print unidecode(name)
    except KeyError:
        name = "Eventbrite"
    
    try:
        geoDataLocal = markerGeocode(value['venue']['address']['localized_address_display']) #This helps google find locations based on the crap DataSF Locations
    except KeyError:
        geoDataLocal = ['KeyError','KeyError','KeyError','KeyError']
    
    address = geoDataLocal[3]
    placeId = geoDataLocal[2]
    print address, placeId
    lat = geoDataLocal[0]
    lng = geoDataLocal[1]
    markerType = "Eventbrite_SF"
    try:
        info = value['description']['text']
        #Trim to extended tweet length and the dots (277) or short (144+3??)
        if info is not None:
            info = (info[:147] + '...') if len(info) > 277 else info
        else:
            info = ""
    except KeyError:
        info = ""
    try:
        source = value['url']
    except KeyError:
        source = "http://www.eventbrite.com"
    
    
    
    data = newLayerMarker(placeId, name, address, lat, lng, markerType, info, source, newMarkerData)
    
    #Take the new data, toss it into string format
    mydata = ET.tostring(data)
    
    #Open, write, close the XML file every loop so we get progressive saving
    myfile = open(xmlFilename, "w")  
    myfile.write(mydata)  
    myfile.close()
    
    time.sleep(0.05) #So we don't spam the servers, mostly google

    

