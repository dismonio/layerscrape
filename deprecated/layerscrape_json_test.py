# -*- coding: utf-8 -*-
"""
Created on Sat Feb 03 18:24:55 2018

@author: SamXPS-PC
"""

import json #For json parsing
import xml.etree.ElementTree as ET #for XML manipulation
import xml.dom.minidom
from urllib import urlopen #For pulling in URLs from the web
import os #For seeing if files exist

#import simplejson

importFeed = 1
url = 'https://gbfs.fordgobike.com/gbfs/en/station_information.json'
markerType = "Ford_GoBike"

if importFeed:
    jsonData = urlopen(url).read()
    jsonData = json.loads(jsonData)
else:
    with open('station_information.json') as f:  
        jsonData = json.load(f)
 
#print jsonData
xmlFilename = "Ford_GoBike.xml"
    
### XML SECTION
# Tutorial - http://stackabuse.com/reading-and-writing-xml-files-in-python/
#ElementTree exports and sorts lexiconically, don't try pointless changing that against W3C standards

#Info portion of the xml, should pull some description that is a tweet 2.0 length or less

#Init the main XML tree thing
newMarkerData = ET.Element('markers')
xmlAlreadyExists = os.path.isfile('./' + xmlFilename)
if xmlAlreadyExists:
    print "XML file already exists!"
    tree = ET.parse(xmlFilename) 
    print tree


#Search through the JSON for needed sections
print jsonData.keys()

stationList = jsonData.keys()
print len(stationList)

#print jsonData.items()
print jsonData['data']['stations'][1]['name']
jsonDataStations = jsonData['data']['stations']

print len(jsonDataStations)
#print jsonDataStations


for value in jsonDataStations:
    placeId = value['station_id']
    name = value['name'] + " (Bike Capacity: " + str(value['capacity']) +")"
    address = str(float(value['lat'])) + ", " + str(float(value['lon']))
    lat = str(float(value['lat']))
    lng = str(float(value['lon']))
    markerType = "Ford_GoBike"
    info = "Bike Capacity " + str(value['capacity'])
    source = value['rental_url']
    
    
    
    data = newLayerMarker(placeId, name, address, lat, lng, markerType, info, source, newMarkerData)
    
    #Take the new data, toss it into string format
    mydata = ET.tostring(data)
    
    #Open, write, close the XML file every loop so we get progressive saving
    myfile = open(xmlFilename, "w")  
    myfile.write(mydata)  
    myfile.close() 

    
#Sets up an XML element specific to Layercake's xml structure
def newLayerMarker(placeId, name, address, lat, lng, markerType, info, source, newMarkerData):
    
    #Inits a new marker line item in the XML data
    item = ET.SubElement(newMarkerData, 'marker') 
    
    #Create each element of the marker
    #item.set('id', markerId) #Ommitted because it's a pointless metric
    item.set('name', name)  
    item.set('address', address)
    item.set('lat', lat)
    item.set('lng', lng)
    item.set('type', markerType)
    item.set('info', info)
    item.set('source', source)
    item.set('id', placeId)
       
    return (newMarkerData)