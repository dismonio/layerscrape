# -*- coding: utf-8 -*-
"""
Created on Sat Feb 03 18:07:36 2018

@author: SamXPS-PC
"""


#The point of all this is to scrape web content for Layercake in this program known as Layerscrape
#It is laid out, generally, as a function that sees if an XML file already exists for the data source
#Then it goes and generates a list of pages it needs to scrape
#
#The way it generates the page list to scrape is by parsing a single page of results, then going to their respective links
#The search results currently are manually generated (as a "save as" html result from a browser) so that is parsed locally
#As it parses, it appends the xml file in case it fails. If it does fail, there is minimal exception handling, except by ctrl+f for Apple HQ lat/lng
#From there, you can manually correct the results
#
#
#This program will probably need to be tailored quite a bit per data source, but the actual scraping is not complicated
#Take advantage of a few switches I've programmed, mostly "importResultsList". I've tried to keep these function switches at the top


# import libraries
#from mechanize import Browser #This is to support reporting browser header info for crawling

import requests
from bs4 import BeautifulSoup  #If BeautifulSoup isn't up to it: http://sangaline.com/post/advanced-web-scraping-tutorial/
import xml.etree.ElementTree as ET
import json
import time
from unidecode import unidecode
import os



#######################################
#######################################
## THIS SECTION REQUIRES USER CONFIG ##
#######################################
#######################################
dataType = "JSON"
xmlFilename = "Ford_GoBike_SF1.xml"

#What do you want to name these markers? May have to cross-check for conflicts
markerType = "Ford_GoBike"
#from scraperBringFido import getPageUrlList
crawlerActive = 0

#Google API keys, these need to be updated when pushed to production
googleGeocodeKey = 'AIzaSyB8qtqib7zL9Xu4eKAClcXAeWNTTxpREn8'
googlePlacesKey = 'AIzaSyB_RV9jTPOHd7Xx-KBta-oT0pSrKeNYGJ0'
addressTest = 0

#Is this a local page?
localHtml = 0
url = r"https://gbfs.fordgobike.com/gbfs/en/station_information.json"

#Do you want to call the function to go find a list of pages for parsing? If not, use an already defined list
importResultsList = 0

baseUrl = "https://gbfs.fordgobike.com/gbfs/en/station_information.json"
pageListUrl = "https://gbfs.fordgobike.com/gbfs/en/station_information.json"

if importResultsList:
    from generatePages import getPageUrlList
    pageIdList = getPageUrlList(pageListUrl)
else:
    pageIdList = ["https://gbfs.fordgobike.com/gbfs/en/station_information.json"] #Manual page entry for debugging

#######################################
#######################################
##### END OF USER CONFIG SECTION ######
#######################################
#######################################



##Looks for the same info on each of these pages and IDs (hopefully those will be exported from our spider bwaha)
def pageSoup(remoteUrl):
    ##Get BeautifulSoup crackin
    #Tutorial: https://www.dataquest.io/blog/web-scraping-tutorial-python/
    if localHtml:
        localUrl = url
        #Local pages have to be loaded a little differently, since there's no server to connect with
        pageLocal = open(localUrl)
        soupLocal = BeautifulSoup(pageLocal.read())
        
    else:
        pageLocal = requests.get(remoteUrl) #This page URL needs to be dynamic for a crawler
        soupLocal = BeautifulSoup(pageLocal.content, 'html.parser')
    
    return soupLocal
    

### XML SECTION
# Tutorial - http://stackabuse.com/reading-and-writing-xml-files-in-python/
#ElementTree exports and sorts lexiconically, don't try pointless changing that against W3C standards

#Info portion of the xml, should pull some description that is a tweet 2.0 length or less
info = "empty"


#Init the main XML tree thing
newMarkerData = ET.Element('markers')
xmlAlreadyExists = os.path.isfile('./' + xmlFilename)
if xmlAlreadyExists:
    print "It exists!"
    tree = ET.parse(xmlFilename) 
    print tree


#Sets up an XML element specific to Layercake's xml structure
def newLayerMarker(name, address, lat, lng, markerType, info, source, placeId, newMarkerData):
    
    #Inits a new marker line item in the XML data
    item = ET.SubElement(newMarkerData, 'marker') 
    
    #Create each element of the marker
    #item.set('id', markerId) #Ommitted because it's a pointless metric
    item.set('name', name)  
    item.set('address', address)
    item.set('lat', lat)
    item.set('lng', lng)
    item.set('type', markerType)
    item.set('info', info)
    item.set('source', source)
    item.set('place_id', placeId)
    

    
    return (newMarkerData)


#Looks up info from google's Geocode API- lat, longs, clean names, PlaceId, etc    
def markerGeocode(address):
    googleApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json'  

    #This passes the 'address' argument through and appends the Google Api key defined at the top
    #There's probably some slick params that can be passed through as well
    #I tried using the google maps python api client, but it seemed waaaay overkill, see aborted function above
    params = {
        'address': address,
        'key': googleGeocodeKey
    }
    
    #Defines the JSON request and puts the response to a variable
    req = requests.get(googleApiUrl, params=params)
    res = req.json() #Here's the JSON Response

    #Exception handling for Geocode responses. If it doesn't have content, point to Apple HQ
    try:
        result = res['results'][0] # Hopefully just discard {u'status': u'OK'
    
    except IndexError: #If you get an indexError (like from a 403 or invalid address, make it point to Apple HQ)
        result = {u'geometry': {u'location_type': u'GEOMETRIC_CENTER', u'bounds': {u'northeast': {u'lat': 37.3335129, u'lng': -122.028577}, u'southwest': {u'lat': 37.3305164, u'lng': -122.0321699}}, u'viewport': {u'northeast': {u'lat': 37.3335129, u'lng': -122.028577}, u'southwest': {u'lat': 37.3305164, u'lng': -122.0321699}}, u'location': {u'lat': 37.3320003, u'lng': -122.0307812}}, u'address_components': [{u'long_name': u'Infinite Loop', u'types': [u'route'], u'short_name': u'Infinite Loop'}, {u'long_name': u'Cupertino', u'types': [u'locality', u'political'], u'short_name': u'Cupertino'}, {u'long_name': u'Santa Clara County', u'types': [u'administrative_area_level_2', u'political'], u'short_name': u'Santa Clara County'}, {u'long_name': u'California', u'types': [u'administrative_area_level_1', u'political'], u'short_name': u'CA'}, {u'long_name': u'United States', u'types': [u'country', u'political'], u'short_name': u'US'}, {u'long_name': u'95014', u'types': [u'postal_code'], u'short_name': u'95014'}], u'place_id': u'ChIJ-7m057a1j4AR2VBPVzJDemk', u'formatted_address': u'Infinite Loop, Cupertino, CA 95014, USA', u'types': [u'route']}

    
    formattedAddress = str(unidecode(result['formatted_address'])) ##This needs to be converted to ASCII strictly because even unicode 8 breaks the str()
                                                                    ##UnicodeEncodeError: 'ascii' codec can't encode character u'\u2019' in position 4: ordinal not in range(128)
                                                                    ## Apparently google's API will respond with UTF-8 characters potentially  
    latLocal = str(result['geometry']['location']['lat'])
    lngLocal = str(result['geometry']['location']['lng'])
    placeIdLocal = str(result['place_id'])
    
    #print('{address}. (lat, lng) = ({lat}, {lng})'.format(**geodata))
    return (latLocal, lngLocal, placeIdLocal, formattedAddress)

#Specific to BringFido.com - This is the scraper section unique to their page layout
def scraperBringFido(remoteUrl):    
    soup = pageSoup(remoteUrl)
    
    markerId = remoteUrl[37:] #This offset accounts for the url length before ID, https://www.bringfido.com/restaurant/, 37 characters
    print markerId

    #Start parsing page data
    #This needs to handle exceptions or it poops itself
    allNames = soup.find_all(itemprop="name") #Report all the page elements matching this in a list
    print "allNames" 
    print soup
    nameLocal = allNames[6].get_text()   #This narrows in on the specific element ID on BringFido's restaurant page format, simplify text
    
    if addressTest:
        addressLocal = 'One Infinite Loop Cupertino, CA 95014'
        
    else:
        addressLocal = soup.find(itemprop="address").get_text()
    
    return (markerId, nameLocal, addressLocal)


#This function runs through each iteration of pages to parse and pulls names and addresses to lookup
for pageIdLocal in pageIdList:
    #Reassign the markerId to the pageId because that seems more useful than counting elements
    
    remoteUrl = pageIdLocal
    scraperResults = scraperBringFido(remoteUrl) #Returns 0=markerId, 1=Name, 2=Address
    
    sourceLocal = baseUrl+scraperResults[0]
    
    geoDataLocal = markerGeocode(scraperResults[2]) #Returns a dict, 0=lat, 1 = long, 2 = place_id, 3 = formatted address
    
    print "Marker: ", scraperResults[0], "  Name: ", scraperResults[1], geoDataLocal[3], " Source: ", sourceLocal
    data = newLayerMarker(scraperResults[1], geoDataLocal[3], geoDataLocal[0], geoDataLocal[1], markerType, info, sourceLocal, geoDataLocal[2], newMarkerData)
    
    #Take the new data, toss it into string format
    mydata = ET.tostring(data)
    
    #Open, write, close the XML file every loop so we get progressive saving
    myfile = open(xmlFilename, "w")  
    myfile.write(mydata)  
    myfile.close()
    
    time.sleep(0.1)
