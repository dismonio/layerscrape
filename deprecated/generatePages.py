# -*- coding: utf-8 -*-
"""
Created on Mon Jan 01 19:01:15 2018

@author: Sam Steele
"""

# import libraries
#from mechanize import Browser #This is to support reporting browser header info for crawling

import requests
from bs4 import BeautifulSoup  #If BeautifulSoup isn't up to it: http://sangaline.com/post/advanced-web-scraping-tutorial/
import xml.etree.ElementTree as ET


#######################################
#######################################
## THIS SECTION REQUIRES USER CONFIG ##
#######################################
#######################################


#Is this a local page?
localHtml = 1
url = r"C:/Users/SamXPS-PC/layerscrape/page_dumps/BringFido_SF_GreaterArea.html"

#Do you want to call the function to go find a list of pages for parsing? If not, use an already defined list
scrapeResultsList = 1
baseUrl = "https://www.bringfido.com/restaurant/"
#pageListUrl = r"C:/Users/SamXPS-PC/layerscrape/page_dumps/BringFido_SF.html"

def getPageIdList():
    print "getPageIdList called"

    if scrapeResultsList:
        pageIdList = getPageUrlList(url)
        print pageIdList
    else:
        pageIdList = ["https://www.bringfido.com/restaurant/5982", "https://www.bringfido.com/restaurant/39422"] #Manual page entry for debugging
    print pageIdList
    return pageIdList



###Html page crawler
#searchPage = requests.get("https://www.bringfido.com/restaurant/city/san_francisco_ca_us/?distance=5&sort=name")
#
#data = searchPage.text
#
#soup = BeautifulSoup(data)
#
#moreResultsButton = soup.find(type="button")
#print moreResultsButton
#
#with requests.Session() as session:
#    response = session.post("https://www.bringfido.com/restaurant/city/san_francisco_ca_us/?distance=5&sort=name", data={'continuation':24}, )
#    #print(response.content)
#
##for link in soup.find_all('a'):
##    print(link.get('href'))


##Looks for the same info on each of these pages and IDs (hopefully those will be exported from our spider bwaha)
def pageSoup(baseUrl, pageListUrl):
    ##Get BeautifulSoup crackin
    #Tutorial: https://www.dataquest.io/blog/web-scraping-tutorial-python/
    if localHtml:
        localUrl = url
        #Local pages have to be loaded a little differently, since there's no server to connect with
        pageLocal = open(localUrl)
        soupLocal = BeautifulSoup(pageLocal.read())
        
    else:
        pageLocal = requests.get(baseUrl) #This page URL needs to be dynamic for a crawler
        soupLocal = BeautifulSoup(pageLocal.content, 'html.parser')
    
    return soupLocal

def getPageUrlList(pageListUrl): 
    soupPage = pageSoup(baseUrl, pageListUrl)
    
    #Start parsing page data
    #This needs to handle exceptions or it poops itself
    pageUrlList = [] #This is an empty list
    pageUrlSet = set()                          #This is an empty set, which only allows distinct items
    for product in soupPage.find_all('h2'):      #for each item in the list
        if product.a["href"] not in pageUrlSet: #if it is not in the set, add it and append it
            #I don't know why this works, no touchy
            pageUrlSet.add(product.a["href"])
            pageUrlList.append(product.a["href"])
    return pageUrlList


print getPageIdList()
print len(getPageIdList())



