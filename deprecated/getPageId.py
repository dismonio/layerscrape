# -*- coding: utf-8 -*-
"""
Created on Mon Jan 01 19:01:15 2018

@author: Sam Steele
"""

# import libraries
#from mechanize import Browser #This is to support reporting browser header info for crawling

import requests
from bs4 import BeautifulSoup  #If BeautifulSoup isn't up to it: http://sangaline.com/post/advanced-web-scraping-tutorial/
import xml.etree.ElementTree as ET


#######################################
#######################################
## THIS SECTION REQUIRES USER CONFIG ##
#######################################
#######################################


#Is this a local page?
localHtml = 0
url = r"C:/Users/SamXPS-PC/layerscrape/page_dumps/5183.html"

#Do you want to call the function to go find a list of pages for parsing? If not, use an already defined list
scrapeResultsList = 0
baseUrl = "https://www.bringfido.com/restaurant/"
pageListUrl = r"C:/Users/SamXPS-PC/layerscrape/page_dumps/BringFido_SF.html"

def getPageIdList():
    print "fully called"

    if scrapeResultsList:
        print "yoyoyo"
        pageIdList = ["0000", "0001"]
    else:
        pageIdList = ["5184", "5183"] #Manual page entry for debugging
    return pageIdList



##Html page crawler
searchPage = requests.get("https://www.bringfido.com/restaurant/city/san_francisco_ca_us/?distance=5&sort=name")

data = searchPage.text

soup = BeautifulSoup(data)

moreResultsButton = soup.find(type="button")
print moreResultsButton

with requests.Session() as session:
    response = session.post("https://www.bringfido.com/restaurant/city/san_francisco_ca_us/?distance=5&sort=name", data={'continuation':24}, )
    #print(response.content)

#for link in soup.find_all('a'):
#    print(link.get('href'))


##Looks for the same info on each of these pages and IDs (hopefully those will be exported from our spider bwaha)
def pageSoup(baseUrl, pageIdLocal):
    ##Get BeautifulSoup crackin
    #Tutorial: https://www.dataquest.io/blog/web-scraping-tutorial-python/
    if localHtml:
        localUrl = url
        #Local pages have to be loaded a little differently, since there's no server to connect with
        pageLocal = open(localUrl)
        soupLocal = BeautifulSoup(pageLocal.read())
        
    else:
        pageLocal = requests.get(baseUrl + pageIdLocal) #This page URL needs to be dynamic for a crawler
        soupLocal = BeautifulSoup(pageLocal.content, 'html.parser')
    
    return soupLocal
    


#for pageIdLocal in pageIdList:
#    #Reassign the markerId to the pageId because that seems more useful than counting elements
#    markerId = pageIdLocal
#    soup = pageSoup(baseUrl, pageIdLocal)
#    
#    
#    #Start parsing page data
#    #This needs to handle exceptions or it poops itself
#    allNames = soup.find_all(itemprop="name") #Report all the page elements matching this in a list
#    nameLocal = allNames[6].get_text()   #This narrows in on the specific element ID on BringFido's restaurant page format, simplify text
#    
#    addressLocal = soup.find(itemprop="address").get_text()
#    sourceLocal = baseUrl+markerId
#    
#    geoDataLocal = markerGeocode(addressLocal) #Returns a dict, 0=lat, 1 = long
#    print geoDataLocal
#    
#    print "Marker: ", markerId, "  Name: ", nameLocal, addressLocal, " Source: ", sourceLocal
#    data = newLayerMarker(markerId, nameLocal, addressLocal, geoDataLocal[0], geoDataLocal[1], markerType, info, sourceLocal, geoDataLocal[2], newMarkerData)
#   


## Create new layer marker, start of a function to be called




