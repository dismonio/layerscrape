# -*- coding: utf-8 -*-
"""
Created on Wed Mar 07 22:44:30 2018

@author: Samuel Steele
@email: sam@sam-steele.com
"""

##Python Master Scraper
##Call this with crontab on the server or whatever you want
##Then use this to manage which scripts are updated

import layerscrape_json_Scoot_Scooters_SF_NG
import layerscrape_json_JumpBike_SF_NG
import layerscrape_json_Ford_GoBike



def scoot_Scooters_SF_NG():
    print "Running layerscrape_json_Scoot_Scooters_SF_NG script"
    print layerscrape_json_Scoot_Scooters_SF_NG.main_execute()
    
def JumpBike_SF_NG():
    print "Running layerscrape_json_Scoot_Scooters_SF_NG script"
    print layerscrape_json_JumpBike_SF_NG.main_execute()
    
def Ford_GoBike():
    print "Running layerscrape_json_Ford_GoBike script"
    print layerscrape_json_Ford_GoBike.main_execute()    
    
scoot_Scooters_SF_NG()
JumpBike_SF_NG()
Ford_GoBike()

print "All 1 minute dynamic xml files updated (hopefully)"