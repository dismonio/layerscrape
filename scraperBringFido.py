# -*- coding: utf-8 -*-
"""
Created on Mon Jan 01 00:34:46 2018

@author: Sam Steele
"""

# import libraries
#from mechanize import Browser #This is to support reporting browser header info for crawling

import requests
from bs4 import BeautifulSoup  #If BeautifulSoup isn't up to it: http://sangaline.com/post/advanced-web-scraping-tutorial/
import xml.etree.ElementTree as ET
import time



def scraperBringFido(remoteUrl):    
    soup = pageSoup(remoteUrl)
    
    markerId = remoteUrl[37:] #This offset accounts for the url length before ID, https://www.bringfido.com/restaurant/, 37 characters
    print markerId
    
    #Start parsing page data
    #This needs to handle exceptions or it poops itself
    allNames = soup.find_all(itemprop="name") #Report all the page elements matching this in a list
    nameLocal = allNames[6].get_text()   #This narrows in on the specific element ID on BringFido's restaurant page format, simplify text
    
    if addressTest:
        addressLocal = 'One Infinite Loop Cupertino, CA 95014'
        
    else:
        addressLocal = soup.find(itemprop="address").get_text()
    
    return (markerId, nameLocal, addressLocal)

    
