# -*- coding: utf-8 -*-
"""
Created on Sat Feb 03 18:24:55 2018
Last Edited: 20180411

@author: Samuel Steele
@email: sam.steele@layercakelabs.com
"""

import json #For json parsing
import xml.etree.ElementTree as ET #for XML manipulation
import xml.dom.minidom
from urllib import urlopen #For pulling in URLs from the web
import os #For seeing if files exist
import shutil #For copying files
import requests #For Google Geocode
import time #For waiting between API Calls
from unidecode import unidecode #To clean up strings
import datetime as dt #For converting datetime into something useful (Eventbrite start/end)
from datetime import datetime
import socket #necessary for exception handling of URL gets
import sys #For exiting the program somewhat gracefully if it fails




def main_execute():
    #Google API keys, these need to be updated when pushed to production
    googleGeocodeKey = 'AIzaSyB8qtqib7zL9Xu4eKAClcXAeWNTTxpREn8'
    googlePlacesKey = 'AIzaSyB_RV9jTPOHd7Xx-KBta-oT0pSrKeNYGJ0'
    
    dir_path = os.path.dirname(os.path.realpath(__file__))
    print "The working directory is: " + dir_path
    
    #Do you want to pull live JSON data?
    importFeed = 1
    
    ############### MANUAL ENTRY REQUIRED HERE##############
    ##key, needs to request sign=true, lat, lon, radius 50mi, page=1000 which actually means give me as close to X results as possible on the page for Meetup's case
    urlBase = 'https://gbfs.fordgobike.com/gbfs/en/station_information.json' #&end_date_range=2018-02-26T23:59:59'
    markerType = "Ford_GoBike"
    xmlFilePrefix = "Ford_GoBike"
    xmlFileSuffix = ".xml"
    xmlFilename = xmlFilePrefix + xmlFileSuffix

    
    ## API Query Parameters
    ## Append search date to url
    
    ## Limit results to not be very far into the past
    #startDateTime = datetime.utcnow() + dt.timedelta(hours=-8)
    #startDateTime = datetime.strftime(startDateTime, '%Y-%m-%dT00:00:00')
    #startDateTime = str(startDateTime)
    startDateTime = '' #No start date time is needed to be included in the query (the Meetup API doesn't seem to respond to this)
    
    ##End Date Time - Filter with a start date before this date
    endDateTime = datetime.utcnow() + dt.timedelta(hours=-8) #This grabs today's date, then puts an offset of X days (UTC -8:00 for PST)
    endDateTime = datetime.strftime(endDateTime, '%Y-%m-%dT23:59:59')
    endDateTime = str(endDateTime) #End the last time of day to the URL so we include the whole day
    #endDateTime = '&EndDateTime=2018-02-26T23:59:59Z' ## Tester for picking a specific time
    

    
        
    ### XML SECTION
    # Tutorial - http://stackabuse.com/reading-and-writing-xml-files-in-python/
    #ElementTree exports and sorts lexiconically, don't try pointless changing that against W3C standards
    
    #Info portion of the xml, should pull some description that is a tweet 2.0 length or less
    
    #Init the main XML tree thing
    newMarkerData = ET.Element('markers')
    xmlAlreadyExists = os.path.isfile(dir_path + '/' + xmlFilename)
    print "Does this file already exist?" + dir_path + '/' + xmlFilename
    print xmlAlreadyExists
    if xmlAlreadyExists:
        print "XML file already exists!"
        xmlFilenameBackup = xmlFilePrefix + "_" + str(datetime.utcnow().strftime("%Y%m%d-%H%M%S")) + xmlFileSuffix
        print xmlFilenameBackup
        
        source_name = os.path.join(dir_path, xmlFilename)
        backup_name = os.path.join(dir_path + '/backup/', xmlFilenameBackup)
        shutil.copy(source_name, backup_name)
        
        print "Making a copy of " + xmlFilename + " and storing it in backup folder as " + backup_name
        #tree = ET.parse(xmlFilename) #This is to append a current file, great for re-parsing and appending a current data set
        #print tree
    if not xmlAlreadyExists:
        xmlFile = open((dir_path + '/' + xmlFilename), "w")  #Make the new temp file to store everything in memory
        xmlFile.close() #Close the temp file
    
    
    
    #Sets up an XML element specific to Layercake's xml structure
    def newLayerMarker(placeId, name, address, lat, lng, markerType, info, source, newMarkerData):
        
        #Inits a new marker line item in the XML data
        item = ET.SubElement(newMarkerData, 'marker') 
        
        #Create each element of the marker
        #item.set('id', markerId) #Ommitted because it's a pointless metric
        item.set('name', name)  
        item.set('address', address)
        item.set('lat', lat)
        item.set('lng', lng)
        item.set('type', markerType)
        item.set('info', info)
        item.set('source', source)
        item.set('id', placeId)
        
        print unidecode(name)
        print unidecode(address)
        print lat + ", " + lng + ", " + placeId
        print unidecode(markerType) 
        print info
        print unidecode(source)
        print " "
    
           
        return (newMarkerData)
    
    #### Google Geocode API if needed ####
    #def markerGeocode(address):
    #    googleApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json'  
    #
    #    #This passes the 'address' argument through and appends the Google Api key defined at the top
    #    #There's probably some slick params that can be passed through as well
    #    #I tried using the google maps python api client, but it seemed waaaay overkill, see aborted function above
    #    params = {
    #        'address': address,
    #        'key': googleGeocodeKey
    #    }
    #    
    #    #Defines the JSON request and puts the response to a variable
    #    req = requests.get(googleApiUrl, params=params)
    #    res = req.json() #Here's the JSON Response
    #
    #    #Exception handling for Geocode responses. If it doesn't have content, point to Apple HQ
    #    try:
    #        result = res['results'][0] # Hopefully just discard {u'status': u'OK'
    #    
    #    except IndexError: #If you get an indexError (like from a 403 or invalid address, make it point to Apple HQ)
    #        result = {u'geometry': {u'location_type': u'GEOMETRIC_CENTER', u'bounds': {u'northeast': {u'lat': 37.3335129, u'lng': -122.028577}, u'southwest': {u'lat': 37.3305164, u'lng': -122.0321699}}, u'viewport': {u'northeast': {u'lat': 37.3335129, u'lng': -122.028577}, u'southwest': {u'lat': 37.3305164, u'lng': -122.0321699}}, u'location': {u'lat': 37.3320003, u'lng': -122.0307812}}, u'address_components': [{u'long_name': u'Infinite Loop', u'types': [u'route'], u'short_name': u'Infinite Loop'}, {u'long_name': u'Cupertino', u'types': [u'locality', u'political'], u'short_name': u'Cupertino'}, {u'long_name': u'Santa Clara County', u'types': [u'administrative_area_level_2', u'political'], u'short_name': u'Santa Clara County'}, {u'long_name': u'California', u'types': [u'administrative_area_level_1', u'political'], u'short_name': u'CA'}, {u'long_name': u'United States', u'types': [u'country', u'political'], u'short_name': u'US'}, {u'long_name': u'95014', u'types': [u'postal_code'], u'short_name': u'95014'}], u'place_id': u'ChIJ-7m057a1j4AR2VBPVzJDemk', u'formatted_address': u'Infinite Loop, Cupertino, CA 95014, USA', u'types': [u'route']}
    #
    #    
    #    formattedAddress = str(unidecode(result['formatted_address'])) ##This needs to be converted to ASCII strictly because even unicode 8 breaks the str()
    #                                                                    ##UnicodeEncodeError: 'ascii' codec can't encode character u'\u2019' in position 4: ordinal not in range(128)
    #                                                                    ## Apparently google's API will respond with UTF-8 characters potentially  
    #    latLocal = str(result['geometry']['location']['lat'])
    #    lngLocal = str(result['geometry']['location']['lng'])
    #    placeIdLocal = str(result['place_id'])
    #    
    #    #print('{address}. (lat, lng) = ({lat}, {lng})'.format(**geodata))
    #    time.sleep(0.05) #So we don't spam the servers, mostly google
    #    return (latLocal, lngLocal, placeIdLocal, formattedAddress)
    
    
    
    #This iterates through our values in our current JSON data list    
    def jsonParser(jsonDataItems):
        for value in jsonDataItems:
            try:
                #print value['station_id'] ##This filters any results that are not 'offsale', previously initialized in an "if" statement
                
                #Loop for associating specifically with Ford GoBike for looking up in a secondary JSON feed with additional info
                station_id = int(value['station_id'])
                #print station_id
                stationStatusInfo = fordGoBikeStationLookup(station_id)
                #print stationStatusInfo
                #station_status = {}
                #print str(jsonDataStationStatus[station_id]['num_bikes_available'])
                     
                try:
                    name = "Ford GoBike Station: " + value['name'] + " (Bikes Available: " + str(int(stationStatusInfo['num_bikes_available']) + int(stationStatusInfo['num_ebikes_available'])) + ")"
                    #print unidecode(name)
                except KeyError:
                    try:
                        name = "Ford GoBike Station: " + value['name']
                    except KeyError:
                        name = "Ford GoBike"
                
                address = ""
                placeId = str(value['station_id'])
                lat = str(value['lat'])
                lng = str(value['lon'])
                markerType = "Ford_GoBike"
                
                try:
                    info4 = str(value['short_name'])
                    info4 = '<b>Station:</b> ' + info4
                except KeyError:
                    info1 = ''
                
                try:
                    info1 = str(stationStatusInfo['num_bikes_available'])
                    info1 = '<br><b>Bikes Available:</b> ' + info1
                except KeyError:
                    info1 = ''
                
                try:
                    info2 = str(stationStatusInfo['num_ebikes_available'])
                    info2 = '<br><b>eBikes Available: </b>' + info2
                except KeyError:
                    info2 = ''
                
                try:
                    info3 = str(stationStatusInfo['num_docks_available'])
                    info3 = '<br><b>Docks Available: </b>' + info3
                except KeyError:
                    info3 = ''
                    
                try:
                    if str(stationStatusInfo['is_installed']) == '0':
                        info6 = '<br><br><b>This station may not be available yet!</b>'
                    else:
                        info6 = ''
                except KeyError:
                    info6 = ''
                
                
                try:
                    
                    #If Linux timestamp didn't work, try the Windows version
                    #Need to add a # symbol in Windows, '-' if Linux to %I so it's %#I or %-I 
                    try:
                        #Last reported update from station in POSIX time
                        info5 = datetime.fromtimestamp(float(str(stationStatusInfo['last_reported']))) + dt.timedelta(hours=-7)
                        info5 = datetime.strftime(info5, "%-I:%M %p, %a %b %d")
                    except ValueError as errorMessage:  #Else try the Windows version
                        if str(errorMessage) != 'Invalid format string':
                            print 'Invalid format string in time calculation of jsonParser'
                        else:
                            info5 = datetime.fromtimestamp(float(str(stationStatusInfo['last_reported']))) + dt.timedelta(hours=-7)
                            info5 = datetime.strftime(info5, "%#I:%M %p, %a %b %d")
                            #info5 = datetime.fromtimestamp(info5).strftime("%#I:%M %p, %a %b %d")
                                                                
                    info5 = '<br><br><small><i>Last Station Report: ' + str(info5) +' (beta)</i></small>'
                except KeyError:
                    info5 = ''
                
                #Last updated function, take now's time
                infoLastUpdated = datetime.utcnow() + dt.timedelta(hours=-7)
                #infoLastUpdated = datetime.strptime(infoLastUpdated, "%Y-%m-%dT%H:%M")
                
                #If Linux timestamp didn't work, try the Windows version
                #Need to add a # symbol in Windows, '-' if Linux to %I so it's %#I or %-I 
                try: #Try for Linux conversion
                    infoLastUpdated = datetime.strftime(infoLastUpdated, "%-I:%M %p, %a %b %d")
                except ValueError as errorMessage:  #Else try the Windows version
                    if str(errorMessage) != 'Invalid format string':
                        print 'Invalid format string in time calculation of jsonParser'
                    else:
                        infoLastUpdated = datetime.strftime(infoLastUpdated, "%#I:%M %p, %a %b %d")
                
                
                infoLastUpdated = '<br><small><i>Marker Last Updated: ' + str(infoLastUpdated) + '</i></small>'
                
                try:
                    info = info4 + info1 + info2 + info3 + info6 + info5 + infoLastUpdated
                    #Trim to extended tweet length and the dots (277) or short (144+3??)
                    if info is not None:
                        info = (info[:277] + '...') if len(info) > 277 else info
                    else:
                        info = ""
                except KeyError:
                    info = ""
                    
                    
                    
                try:
                    source = value['rental_url']
                except KeyError:
                    source = "https://www.fordgobike.com/"
                
                
                
                xmlDataRaw = newLayerMarker(placeId, name, address, lat, lng, markerType, info, source, newMarkerData)
                
                #Take the new data, toss it into string format
                xmlDataString = ET.tostring(xmlDataRaw)
#                print xmlDataRaw
#                    
#                #Make the data legible in the terminal window, this grows exponentially, used for verifying XML real output
#                xmlDataPrint = xml.dom.minidom.parseString(xmlDataString)
#                print xmlDataPrint.toprettyxml()                            
                    

            except KeyError as errorMessage: ##Else statement that is a part of the status code filter
                print 'Generic KeyError for ID: ' + value['station_id'] + " Error: '%s' \n" % str(errorMessage)
                time.sleep(0.05)
                pass
        return xmlDataString
    
    if importFeed:
        
        urlPayload = {
#            'key' : '79182d4950193f3b2b3415a4265d70',
#            'sign': 'true',
#            'lon' : '-122.419416',
#            'lat' : '37.782000',
#            'radius' :'50',
#            #'startDateTime' : startDateTime,
#            'EndDateTime' : endDateTime,
#            'per_page' : '1000'
            }
        urlAuth = {
#                'key' : '79182d4950193f3b2b3415a4265d70',
                }

    
        #httpData = urlopen(urlBase)  #this uses urllib
        httpData = requests.get(urlBase, params=urlPayload) #this uses requests library
        print str(httpData.url) + '\n'
        
        #print httpData.status_code()
        #print "httpStatusCode: "
        #print str(httpStatusCode) + "\n"
        
        httpHeaderLink = httpData.headers.get('link', None)
        if httpHeaderLink is not None:
            httpHeaderLink = httpData.links["next"]['url']
            print "httpHeaderLink: "
            print httpHeaderLink
            getNextPageExists = 1
        
        jsonData = httpData.json()
        #jsonData = json.loads(jsonData) #required for urllib
    else:
        with open('Ford_GoBike.html') as f:  
            jsonData = json.load(f)
    
    
     
        
    
     
    print len(jsonData)
    ##Search through the JSON for needed sections
        #This is for data sets with keys, such as the JSON output from Ford GoBike
        #Comment out if the data is already setup in a single, non-nested dict
    #print jsonData.keys()
    #print type(jsonData)
    
    jsonDataList = jsonData.keys()
    #print len(jsonDataList)
    
    #print jsonData.items()
    #print str(unidecode(jsonData['_embedded']['events'][1]['name']))
    jsonDataItems = jsonData['data']['stations']
    #print jsonDataItems
    
    print "First Page length of jsonDataItems: " + str(len(jsonDataItems)) + '\n'
    
    
    
    def getNextPage(urlPage):

        try:
            httpData = requests.get(urlPage, params=urlAuth)
            jsonData = httpData.json()
        except socket.error as errorMessage:
            time.sleep(5) #Wait 5 seconds to re-try the connection
            httpData = requests.get(urlPage, params=urlAuth)
            jsonData = httpData.json()
            print str(errorMessage)
        except:
            print "Unexpected Error, check /var/speed/mail/ec2-user log"
            
        
        print "Parsing page url: " + str(httpData.url) + '\n'
        
        if httpHeaderLink is not None:
            getNextPageExists = 1
        if httpHeaderLink is None:
            getNextPageExists = 0
        
        #print jsonData.items()
        #print jsonData['events'][1]['name']['text']
        try:
            jsonDataItems = jsonData['items']
        except KeyError:
            print "KeyError on converting the JSON results, setting to 'empty'"  
            jsonDataItems = []
        return jsonDataItems, httpData
    
    
    def xmlFileMaker(xmlDataString):
        try:
            #Take the time now to make the new file as a temp one
            print "Making the temporary xml file \n"
            #print xmlDataString #Use this to test the XML content while it's still in memory before file writing
            print dir_path + '/' + xmlFilename + '.temp' + '\n'
            
            xmlFile = open((dir_path + '/' + xmlFilename + '.temp'), "w")  #Make the new temp file to store everything in memory
            xmlFile.write(xmlDataString)
            xmlFile.close() #Close the temp file
            
            #Delete the old file and immediately rename the new file by removing the .temp extension
            print "Removing the primary file and repacing it with the temporary file - " + xmlFilename
            os.remove(dir_path + '/' + xmlFilename) #Remove the main file
            os.rename((dir_path + '/' + xmlFilename + '.temp'), (dir_path + '/' + xmlFilename)) #Replace with the temporary file by renaming
            
            
        except:
            print "Unexpected error with xmlFileMaker, can't raise due to lack of error handling"
            raise
            
            
    ##################################
    ##Application Specific Functions##
    ##################################

    def fordGoBikeStationStatus():
        urlBaseStationStatus = 'https://gbfs.fordgobike.com/gbfs/en/station_status.json'
        urlPayload = {
#            'key' : '79182d4950193f3b2b3415a4265d70',
#            'sign': 'true',
#            'lon' : '-122.419416',
#            'lat' : '37.782000',
#            'radius' :'50',
#            #'startDateTime' : startDateTime,
#            'EndDateTime' : endDateTime,
#            'per_page' : '1000'
            }
        urlAuth = {
#                'key' : '79182d4950193f3b2b3415a4265d70',
                }

    
        #httpData = urlopen(urlBase)  #this uses urllib
        httpDataStationStatus = requests.get(urlBaseStationStatus, params=urlPayload) #this uses requests library
        print str(httpDataStationStatus.url) + '\n'
        
        #print httpData.status_code()
        #print "httpStatusCode: "
        #print str(httpStatusCode) + "\n" 

        jsonDataStationStatus = httpDataStationStatus.json()
        jsonDataItemsStationStatus = jsonDataStationStatus['data']['stations']
        #print jsonDataItemsStationStatus
        return jsonDataItemsStationStatus
    
    def fordGoBikeStationLookup(lookupStation_id):
        #print jsonDataStationStatus
        for station in jsonDataStationStatus:
            #print station
            #print station['station_id']
            if station['station_id'] == str(lookupStation_id):
                
              station_status = {
                  "station_id": station['station_id'],
                    "num_bikes_available": station['num_bikes_available'],
                    "num_ebikes_available": station['num_ebikes_available'],
                    "num_bikes_disabled": station['num_bikes_disabled'],
                    "num_docks_available": station['num_docks_available'],
                    "num_docks_disabled": station['num_docks_disabled'],
                    "is_installed": station['is_installed'],
                    "is_renting": station['is_renting'],
                    "is_returning": station['is_returning'],
                    "last_reported": station['last_reported'],
                    }
              #print station_status
              break
        else:
            station_status = {}
            print "Station ID lookup failed: " + str(lookupStation_id)
        return station_status
                  

            
            

    
    
    ############################
    ##Main Code Execution Loop##
    ############################
    jsonDataStationStatus = fordGoBikeStationStatus()
    xmlDataString = jsonParser(jsonDataItems)
#    try:
#        xmlDataString = jsonParser(jsonDataItems)
#    except UnboundLocalError:
#        print "jsonParser failed to initialize an xmlDataString"
#        #sys.exit() #This seems aggressive
    
    getNextPageExists = 0 ##There is no next page exists for Meetup, results on one page --This is a huge lie, it's in the HTTP header
    pageCounter = 1
    while getNextPageExists:
        #urlPage = urlBase + '&page=' + str(pageCounter)
        print "While loop page init: " + str(httpHeaderLink) + " \n"
        
        getNextPageData = getNextPage(httpHeaderLink)
        jsonDataItems = getNextPageData[0]
        httpData = getNextPageData[1]
        
        print "Length of jsonDataItems: " + str(len(jsonDataItems)) + " on page " + str(pageCounter) + "\n"
        
        try:
            httpHeaderLink = httpData.links["next"]['url']
    
            if httpHeaderLink is not None:
                pageCounter = pageCounter + 1
                xmlDataString = jsonParser(jsonDataItems)
                httpHeaderLink = httpData.links["next"]['url']
                print "Found an additional page of data!"
                
                print "Going to page: " + str(httpHeaderLink) + "\n"
                getNextPageExists = 1
                time.sleep(0.05) #So we don't spam the servers, mostly google
                
            else:
                getNextPageExists = 0
                print "No more pages"
                
        except KeyError, errorMessage:
            getNextPageExists = 0
            print "No more pages, this is okay, end of pagination, KeyError '%s'." % str(errorMessage)
            
#            try:
            

#            except:
#                print "Issue with xmlFileMaker"
        except:
            print "Unexpected Error, check /var/speed/mail/ec2-user log"
    
    #Commit the XML we've built to a file before shutting down the script
    xmlFileMaker(xmlDataString)        
    print "All done"
   
    
#Must add this to make it externally callable from the master scraper    
if __name__ == '__main__':
    # test1.py executed as script
    # do something
    main_execute()
